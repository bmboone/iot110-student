This is for Lab4:

Notes for setting up Raspbery PI to run Nginx and hand off requests to Flask.

The way I understand it, I need to set it up like this:

Nginx -> uWSGI -> Flask

Installed Nginx:

pi$ sudo apt install nginx
pi$ sudo service nginx start

Go to:

http://10.0.1.45

Got the "Welcome to nginx on Debian!" page

pi$ sudo apt-get install build-essential python-dev

pi$ sudo pip install uwsgi

*** uWSGI is ready, launch it with /usr/local/bin/uwsgi ***



Create a mini Flask app:

pi$ cd
pi$ mkdir sampleApp
pi$ sudo chown www-data sampleApp
pi$ sudo nano sampleApp/sample_app.py



from flask import Flask
first_app = Flask(__name__)

@first_app.route("/")
def first_function():
 return "<html><body><h1 style='color:red'>I am hosted on Raspberry Pi !!</h1></body></html>"

if __name__ == "__main__":
 first_app.run(host='0.0.0.0')




pi$ sudo nano sampleApp/uwsgi_config.ini

[uwsgi]

chdir = /home/pi/sampleApp
module = sample_app:first_app

master = true
processes = 1
threads = 2

uid = www-data
gid = www-data
socket = /tmp/sample_app.sock
chmod-socket = 664
vacuum = true

die-on-term = true


# Test uWSGI initialization

uwsgi --ini /home/pi/sampleApp/uwsgi_config.ini

# Configure NGINX to redirect web traffic to uWSGI

pi$ sudo rm /etc/nginx/sites-enabled/default

pi$ sudo nano /etc/nginx/sites-available/sample_app_proxy

server {
 listen 80;
 server_name localhost;

 location / { try_files $uri @app; }
 location @app {
 include uwsgi_params;
 uwsgi_pass unix:/tmp/sample_app.sock;
 }
}

Then:

pi$ sudo ln -s /etc/nginx/sites-available/sample_app_proxy /etc/nginx/sites-enabled

Restart Nginx

pi$ sudo service nginx restart
