Beacons:

sudo nano /etc/hostname

Put in device name, if not already there:

bb2

# verify hci is available
pi$ hciconfig -h

Output from https://yencarnacion.github.io/eddystone-url-calculator/

-> I entered the main url for my 3-person company, curiointeractive.com


Eddystone URL command calculator

Your commands for "http://curiointeractive.com" are:

$ sudo hciconfig hci0 up

$ sudo hciconfig hci0 leadv 3

$ sudo hcitool -i hci0 cmd 0x08 0x0008 1f 02 01 06 03 03 aa fe 17 16 aa fe 10 00 02 63 75 72 69 6f 69 6e 74 65 72 61 63 74 69 76 65 07


You can find the source for this script at https://github.com/yencarnacion/eddystone-url-calculator


The file beacon-on-iphone.PNG shows a screenshot of this beacon in the Physical Web app.
