Barry Boone
1/18/18
IoT

These client and server python programs are for Homework #2.

This is a Tic-Tac-Toe game, played using a REST API via:
- SenseHat
- a Flask server running on the Pi
- Client python program(s) running on the Mac
- An API to move, as well as to get game status and even undo a previous move.
- You can also just start a new game.

This program automatically detects if there is a winner, or if there is a tie.

It works via PUT, POST, GET, and DELETE.

Player 1 is red; player 2 is blue.

Via the client, players can make moves to the board, which is running on the Pi
as a SenseHat. The game automatically determines when there's a winner, or
when there is a tie.

The basic idea is this:
- Someone launches the Tic-tac-toe server running on the Pi with the SenseHat.
- This is run by launching ttt_server.py
- The server starts out initialized to a new game.
- Two different Tic-tac-toe clients connect to the Pi -- these are the two players.
- When launching ttt_client.py, you indicate the IP address of the ttt server.
- The two players then take turns making their moves. They use a RESTful API to move.
- Moving to square 1, for example, involves sending this command: 
     PUT /ttt/move/1
- If this is player 1 making the move, then a red pixel lights up in the area of square 1
  on the SenseHat. (Note: you have to make sure the SenseHat is oriented correctly for
  the numbering scheme to match up.)
- The game automatically expects the next move to come from player 2, and when player 2
  moves via using their ttt_client.py program, a blue pixel will light up in the area of 
  the tic-tac-toe square they moved to.
- Squares are ordered like this:

    1 | 2 | 3
   ---+---+---
    4 | 5 | 6 
   ---+---+---
    7 | 8 | 9
 
- If during the game you've forgotten whose turn it is, you can issue the command:
   GET /ttt/whomoves
  and the response will let you know
- If you want to check on the status, you can issue this command:
   GET /ttt/status
  and again, the response will tell you the current game status
- It has a 1-level undo. Use:
   DELETE  /ttt/undo
  to remove your last move and then re-play your move
- Once there is a winner or a tie, the SenseHat will automatically detect that and
  display a message on the SenseHat itself indicating who won or if there was a tie.
- You can start a new game via:
   POST /ttt/new

Since the point of the homework was to create a RESTful interface using the Pi as the server
and the Mac as the client, I didn't really build out the game, though it wouldn't be difficult
to do so. Some possible next steps might include:
- Putting in error checking to make sure there are no illegal moves.
- Drawing the tic-tac-toe grid on the SenseHat.
- Putting in an interface to see the current state of the board on request.
etc....

The screenshots included here show the server log and the client log of a game
in progress.

The photo shows the start of a game with red and blue each having made one move.

The video shows red making a move, then the camera switches to show the
results on the SenseHat, with the move being made, and the server detecting that
red has 3-in-a-row and has won. The SenseHat then shows the message (in red)
that reads: "Tic-tac-toe -- red wins!"
