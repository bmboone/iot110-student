For Lab6

I'm just adding a note here that I did attempt to do this lab for AWS,
both in-class and at home, and never did quite get it all working.

Since it seems that for our Q3 classes we will be using Azure, if I understand
that correctly, then this lab is less urgent.

However, I am still very interested in using AWS for IoT and am glad to
have all the notes from this class. I do hope to get to using it in the future.
My company, Curio Interactive, might well use AWS for upcoming projects, so
it's definitely on my radar.
