#!/usr/bin/python
# =============================================================================
#        File : ttt_server.py
# Description : Tic-Tac-Toe on the SenseHat supporting different content types!
#      Author : Barry Boone
#        Date : 1/25/18
#        Notes: This is a tic-tac-toe server for Homework #3.
# =============================================================================

### TODO
#3. Compile protobuf structures
#4. Change client to test receiving json, xml, and protobuf and displaying
#   the response
#5. Test receiving that data on the client side from the server
#6. Send data in using json, xml, and protobuf
#7. Test receiving that data on the server from the client


# Notes: 1. Since there is no error checking, be sure not to make a move onto a
#       previously played square.
#        2. Players alternate moves implicitly: red, then blue, then red...
#        3. There is an API for making moves and checking game status.
#
# API!
# (Note: GET to retrieve info, POST to create, PUT to update, DELETE to delete)
#
# The server can output data in json, xml, plain, and protobuf
#
#        TO MOVE: (PUT)
#        /ttt/move/[1 - 9]
#
# The server can handle input for the above move request in json, xml, plain,
# and protobuf.
#
# Other possible API calls:
#
#        TO START A NEW GAME: (POST)
#        /ttt/new
#
#        TO REQUEST THE GAME STATUS: (GET)
#        /ttt/status
#               IN PROGRESS
#               PLAYER 1 WON
#               PLAYER 2 WON
#               TIE
#
#        TO BE REMINDED WHO GOES NEXT: (GET)
#        /ttt/whomoves
#               PLAYER 1
#               PLAYER 2
#               THE GAME IS OVER
#
#        TO UNDO THE PREVIOUS MOVE: (DELETE)
#        /ttt/undo
#
#        TO GET WHAT'S DISPLAYED ON THE BOARD: (GET)
#       /ttt/board


import random
import string
import json
import sys
import time


PORT = 5000

# Content-Types
type_JSON     = 0
type_XML      = 1
type_PROTOBUF = 2
type_HTML     = 3

content_type = type_HTML # Assume just any regular html-ish thing is fine

from flask import Flask, request

from sense_hat import SenseHat
sense = SenseHat()

num_combos = 8
num_squares = 9

winning_combos = [[1,1,1,0,0,0,0,0,0],
                  [0,0,0,1,1,1,0,0,0],
                  [0,0,0,0,0,0,1,1,1],
                  [1,0,0,1,0,0,1,0,0],
                  [0,1,0,0,1,0,0,1,0],
                  [0,0,1,0,0,1,0,0,1],
                  [1,0,0,0,1,0,0,0,1],
                  [0,0,1,0,1,0,1,0,0]]


# ============================== APIs ====================================

# create the global objects
app = Flask(__name__)

# set up a new game
# newGame()
# newGame() would do what is done here, so I'm just commenting it out for now.
#
# Default to player 1 to move and a fresh board
player = 1
board = [0, 0, 0, 0, 0, 0, 0, 0, 0]
previousMove = 0
sense.clear()

# Make a move
@app.route("/ttt/move/<int:move>", methods=['PUT'])
def TTTMove(move):
  # Move to this square:
  # 1  2  3
  # 4  5  6
  # 7  8  9

  global player
  global board

  # Figure out content type
  initializeContentType(request.headers)

  # Update the board with this move.
  updateBoard(player, move)

  # Show the move on the sensehat
  pixelx, pixely = mapMoveToPixel(move)
  r, g, b = getPlayerColor(player)
  sense.set_pixel(pixelx, pixely, r, g, b)

  # Let's pause here a moment
  time.sleep(3)

  # Get the game status
  status = checkGameStatus()

  msg = ""

  # Do the right thing.
  if (status == 0):
      # Prep next player to move
      player = changeTurn(player)
      msg = "Game in progress..."

  elif (status == 1):
      msg = "Tic-tac-toe -- red wins!"
      sense.show_message(msg, text_colour=[255, 0, 0])
      time.sleep(1)
      sense.clear()

  elif (status == 2):
      # Player 2 has won!
      msg = "Tic-tac-toe -- blue wins!"
      sense.show_message(msg, text_colour=[0, 0, 255])
      time.sleep(1)
      sense.clear()

  else:
      # Tie!
      msg = "Tie!"
      sense.show_message(msg, text_colour=[255, 255, 255])
      time.sleep(1)
      sense.clear()

  rsp_data = ""

  # Send a response
  if (content_type == type_HTML):
    rsp_data = msg

  elif (content_type == type_JSON):
    rsp_data = '{"status":"' + msg + '"}\n'

  elif (content_type == type_XML):
    rsp_data = '<status>' + msg + '</status>\n'

  elif (content_type == "type_PROTOBUF"):
    status = status_pb2.Status()
    status.msg = msg
    mystr = status.SerializeToString()
    rsp_data = base64.b64encode(mystr)

  return rsp_data, 200


@app.route("/ttt/whomoves", methods=['GET'])
def TTTWhoMoves():
   global player

   # Figure out content type
   initializeContentType(request.headers)

   status = checkGameStatus()

   msg = ""

   if (status == 0):
      if (player == 1):
          # Player 1 moves
          msg = "Red to move"

      else:
          # Player 2 moves
          msg = "Blue to move"

   else:
       # The game is over
       msg = "Game over"


   # Send a response
   rsp_data = ""

   if (content_type == type_HTML):
     rsp_data = msg

   elif (content_type == type_JSON):
     rsp_data = '{"status":"' + msg + '"}\n'

   elif (content_type == type_XML):
     rsp_data = '<status>' + msg + '</status>\n'

   elif (content_type == "type_PROTOBUF"):
     status = status_pb2.Status()
     status.msg = msg
     mystr = status.SerializeToString()
     rsp_data = base64.b64encode(mystr)

   return rsp_data, 200

@app.route("/ttt/status", methods=['GET'])
def TTTStatus():
   # Figure out content type
   initializeContentType(request.headers)

   status = checkGameStatus()

   if (status == 0):
      # IN PROGRESS
      msg = "Game in progress"

   elif (status == 1):
      # PLAYER 1 WON
      msg = "Red won"

   elif (status == 2):
       # PLAYER 2 WON
       msg = "Blue won"

   else:
       # TIE
       msg = "Tie"

   # Send a response
   rsp_data = ""

   if (content_type == type_HTML):
     rsp_data = msg

   elif (content_type == type_JSON):
     rsp_data = '{"status":"' + msg + '"}\n'

   elif (content_type == type_XML):
     rsp_data = '<status>' + msg + '</status>\n'

   elif (content_type == "type_PROTOBUF"):
     status = status_pb2.Status()
     status.msg = msg
     mystr = status.SerializeToString()
     rsp_data = base64.b64encode(mystr)

   return rsp_data, 200

@app.route("/ttt/new", methods=['POST'])
def TTTNew():

    # Figure out content type
    initializeContentType(request.headers)

    newGame()

    msg = "New game started"

   # Send a response
   rsp_data = ""

   if (content_type == type_HTML):
     rsp_data = msg

   elif (content_type == type_JSON):
     rsp_data = '{"status":"' + msg + '"}\n'

   elif (content_type == type_XML):
     rsp_data = '<status>' + msg + '</status>\n'

   elif (content_type == "type_PROTOBUF"):
     status = status_pb2.Status()
     status.msg = msg
     mystr = status.SerializeToString()
     rsp_data = base64.b64encode(mystr)

   return rsp_data, 200

@app.route("/ttt/undo", methods=['DELETE'])
def TTTUndo():

    # Figure out content type
    initializeContentType(request.headers)

    undo()

    msg = "Last move deleted"

    # Send a response
    rsp_data = ""

    if (content_type == type_HTML):
      rsp_data = msg

    elif (content_type == type_JSON):
      rsp_data = '{"status":"' + msg + '"}\n'

    elif (content_type == type_XML):
      rsp_data = '<status>' + msg + '</status>\n'

    elif (content_type == "type_PROTOBUF"):
        status = status_pb2.Status()
        status.msg = msg
        mystr = status.SerializeToString()
        rsp_data = base64.b64encode(mystr)

    return rsp_data, 200

@app.route("/ttt/board", methods=['GET'])
def TTTBoard():
    global board

    # Figure out content type
    initializeContentType(request.headers)

    rsp_data = ""

    # Construct the board differently for each type of putput.
    if (content_type == "type_HTML"):

        #    | x | o
        # ---+---+---
        #    | x |
        # ---+---+---
        #    | o |

       rsp_data += getBoardSquareAsPlainText(1)
       rsp_data += "|"
       rsp_data += getBoardSquareAsPlainText(2)
       rsp_data += "|"
       rsp_data += getBoardSquareAsPlainText(3)
       rsp_data += "\n"

       rsp_data += "---+---+---\n"

       rsp_data += getBoardSquareAsPlainText(4)
       rsp_data += "|"
       rsp_data += getBoardSquareAsPlainText(5)
       rsp_data += "|"
       rsp_data += getBoardSquareAsPlainText(6)
       rsp_data += "\n"

       rsp_data += "---+---+---\n"

       rsp_data += getBoardSquareAsPlainText(7)
       rsp_data += "|"
       rsp_data += getBoardSquareAsPlainText(8)
       rsp_data += "|"
       rsp_data += getBoardSquareAsPlainText(9)
       rsp_data += "\n"

    elif (content_type == "type_JSON"):
        # {"board":{{"row1":"_xo"},{"row2":"_x_"},{"row3":"_o_"}}}
        rsp_data = '{"board":{{"row1":'
        rsp_data += getBoardSquareAsChar(1)
        rsp_data += getBoardSquareAsChar(2)
        rsp_data += getBoardSquareAsChar(3)

        rsp_data += '"},{"row2":"'

        rsp_data += getBoardSquareAsChar(4)
        rsp_data += getBoardSquareAsChar(5)
        rsp_data += getBoardSquareAsChar(6)

        rsp_data += '"},{"row3":"'

        rsp_data += getBoardSquareAsChar(7)
        rsp_data += getBoardSquareAsChar(8)
        rsp_data += getBoardSquareAsChar(9)

        rsp_data = '"}}}\n'

    elif (content_type == "type_XML"):
        # <?xml version="1.0" encoding="UTF-8"?>
        # <board>
        #   <row1>_xo</row1>
        #   <row2>_x_</row2>
        #   <row3>_o_</row3>
        # </board>

        rsp_data = '<?xml version="1.0" encoding="UTF-8"?>\n'
        rsp_data += '<board>\n'

        rsp_data += '  <row1>'
        rsp_data += getBoardSquareAsChar(1)
        rsp_data += getBoardSquareAsChar(2)
        rsp_data += getBoardSquareAsChar(3)
        rsp_data += '</row1>\n'

        rsp_data += '  <row2>'
        rsp_data += getBoardSquareAsChar(4)
        rsp_data += getBoardSquareAsChar(5)
        rsp_data += getBoardSquareAsChar(6)
        rsp_data += '</row2>\n'

        rsp_data += '  <row3>'
        rsp_data += getBoardSquareAsChar(7)
        rsp_data += getBoardSquareAsChar(8)
        rsp_data += getBoardSquareAsChar(9)
        rsp_data += '</row3>\n'

        rsp_data += "</board>\n"

    elif (content_type == "type_PROTOBUF"):
        protoBoard = board_pb2.Board()
        protoBoard.s1 = board[0]
        protoBoard.s2 = board[1]
        protoBoard.s3 = board[2]
        protoBoard.s4 = board[3]
        protoBoard.s5 = board[4]
        protoBoard.s6 = board[5]
        protoBoard.s7 = board[6]
        protoBoard.s8 = board[7]
        protoBoard.s9 = board[8]

        mystr = protoBoard.SerializeToString()
        rsp_data = base64.b64encode(mystr)

    return rsp_data, 200


########################################
# Helper functions
########################################

# Given a square 1 - 9, get it's symbol for making a nice display
# of the board in plain text.
def getBoardSquareAsPlainText(i):
    global board

    x = board[i - 1]

   if (x == 0):
       return "   "
   elif (x == 1):
       return " x "
   else:
       return " o "

def getBoardSquareAsChar(i):
    global board

    x = board[i - 1]

   if (x == 0):
       return "_"
   elif (x == 1):
       return "x"
   else:
       return "o"

def newGame():
   global player
   global board
   global previousMove

   player = 1
   board = [0, 0, 0, 0, 0, 0, 0, 0, 0]
   sense.clear()
   previousMove = 0

def updateBoard(p, m):
  global board
  global previousMove

  # move player x to square m
  board[m - 1] = p
  previousMove = m

def undo():
  global previousMove
  global player

  # Only 1 level deep -- implemented to show an example of DELETE in the API.
  updateBoard(0, previousMove)
  player = changeTurn(player)

  # Turn off that pixel
  x, y = mapMoveToPixel(previousMove)
  sense.set_pixel(x, y, 0, 0, 0)

def checkGameStatus():
   global num_combos
   global num_squares
   global winning_combos
   global board

   # 0 = in progress still
   # 1 = player 1 won
   # 2 = player 2 won
   # 3 = tie, game over
   status = 0

   # First check player 1 for winning combo
   for combo in xrange(0, num_combos):
       ttt = 0
       for square in xrange(0, num_squares):
          if (winning_combos[combo][square] > 0 and board[square] == 1):
             ttt += 1
          if (ttt == 3):
              # tic-tac-toe, 3 in a row!
              status = 1

   # Then check player 2 for winning combo if status is still 0
   if (status == 0):
      for combo in xrange(0, num_combos):
         ttt = 0
         for square in xrange(0, num_squares):
            if (winning_combos[combo][square] > 0 and board[square] == 2):
               ttt += 1
            if (ttt == 3):
                # tic-tac-toe, 3 in a row!
                status = 2

   # Then check for tie if status is still 0
   if (status == 0):
      if (0 not in board):
          # All positions are filled in the board
          status = 3

   return status


def changeTurn(p):
    if (p == 1):
        return 2
    else:
        return 1

def getPlayerColor(p):
    if (p == 1):
        return 255, 0, 0 # player 1 is red
    else:
        return 0, 0, 255 # player 2 is blue

def mapMoveToPixel(move):
   if (move == 1):
       return 1, 1
   elif (move == 2):
       return 4, 1
   elif (move == 3):
       return 7, 1
   elif (move == 4):
       return 1, 4
   elif (move == 5):
       return 4, 4
   elif (move == 6):
       return 7, 4
   elif (move == 7):
       return 1, 7
   elif (move == 8):
       return 4, 7
   else:
       return 7, 7

# Routine to figure out the content type and set globals
def initializeContentType(rh):
  global content_type

  content_json      = 'application/json'
  content_xml       = 'application/xml'
  content_protobufs = 'application/protobuf'

  # assume JSON
  content_type = type_HTML

  if ('Content-Type' in rh):
      if (rh['Content-Type'] == content_xml):
          content_type = type_XML
      elif (rh['Content-Type'] == content_protobufs):
          content_type = type_PROTOBUF
      elif (rh['Content-Type'] == content_json):
          content_type = type_JSON


# ============================== Main ====================================

if __name__ == "__main__":

  print("HTTP Server")

  app.debug = True
  app.run(host='0.0.0.0', port=PORT)
