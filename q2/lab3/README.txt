Barry Boone
1/25/18
IoT
Homework #3

This has been superseded by the lab3 folder. This (lab3a) is obsolete.

---------

This is based on the tic-tac-toe client and server python programs I wrote
for Homework #2.

As a quick overview, as explained in the Readme file for Homework #2,
this is a Tic-Tac-Toe game, played using a REST API.

This program automatically detects if there is a winner, or if there is a tie.

It works via PUT, POST, GET, and DELETE.

Player 1 is red; player 2 is blue.

Squares are ordered like this:

    1 | 2 | 3
   ---+---+---
    4 | 5 | 6
   ---+---+---
    7 | 8 | 9

The API is:
   PUT /ttt/move/[1-9]
   GET /ttt/whomoves
   GET /ttt/status
   DELETE  /ttt/undo
   POST /ttt/new

For Homework #3, I added support for XML, JSON, and Google Protobufs.

To make this more sophisticated, I added an API GET call to retrieve the
board.

   GET /ttt/board

This returns:
row1: col1, col2, col3
row2: col1, col2, col3
row3: col1, col2, col3

Example:

   | x | o
---+---+---
   | x |
---+---+---
   | o |

 This shows a board where each player has moved twice, and it appears that
 player 2, playing o, probably blocked player 1's winning move by filling
 square 8 (row3, col2).

response in plain text/html
   | x | o
---+---+---
   | x |
---+---+---
   | o |

response in json:
{"board":{{"row1":"_xo"},{"row2":"_x_"},{"row3":"_o_"}}}

response in xml:
<?xml version="1.0" encoding="UTF-8"?>
<board>
   <row1>_xo</row1>
   <row2>_x_</row2>
   <row3>_o_</row3>
</board>

schema:
<?xml version="1.0" encoding="UTF-8" ?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

<xs:element name="board">
  <xs:complexType>
    <xs:element name="row1" type="xs:string" use="required"/>
    <xs:element name="row2" type="xs:string" use="required"/>
    <xs:element name="row3" type="xs:string" use="required"/>
  </xs:complexType>
</xs:element>
