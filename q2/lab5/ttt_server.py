#!/usr/bin/python
# =============================================================================
#        File : ttt_server.py
# Description : Tic-Tac-Toe on the SenseHat supporting Dropbox!
#      Author : Barry Boone
#        Date : 2/8/17
#        Notes: This is a tic-tac-toe server for Homework #4.
# In this version, I've added an API so that it can write out board info
# to a file in a folder on Dropbox, and then retrieve that board info to
# reset the game to what is saved in that file.
# The folder is at bb-ttt in my dropbox account.
# =============================================================================
# Notes:  1. There is an API for making moves and checking game status.
#         2. Players alternate moves implicitly: red (X), then blue (O),
#            then red (X), ...
#         3. Since there is no error checking yet, be sure not to make
#           a move onto a previously played square!

#
# API!
# (Note: GET to retrieve info, POST to create, PUT to update, DELETE to delete)
#
#        ORIGINAL API from Homework from Lab 2:
#
#        TO MOVE: (PUT)
#        /ttt/move/[1 - 9]
#
#        TO START A NEW GAME: (POST)
#        /ttt/new
#
#        TO REQUEST THE GAME STATUS: (GET)
#        /ttt/status
#               IN PROGRESS
#               PLAYER 1 WON
#               PLAYER 2 WON
#               TIE
#
#        TO BE REMINDED WHO GOES NEXT: (GET)
#        /ttt/whomoves
#               PLAYER 1
#               PLAYER 2
#               THE GAME IS OVER
#
#        TO UNDO THE PREVIOUS MOVE: (DELETE)
#        /ttt/undo
#
#        NEW FOR HOMEWORK 3 to show an API call supporting XML, JSON, and
#        Protobufs:  Get and Set a Square.
#        Send in the appropriate data -- the number of the square on the
#        board -- in the appropriate format.
#        Squares are numbered like this:
#
#            1  |  2  |  3
#           ----+-----+----
#            4  |  5  |  6
#           ----+-----+----
#            7  |  8  |  9
#
#        GET /ttt/square
#               Gets the current value in the given square: 1 (X),
#               2 (O), or 0 (empty)
#               Send in the square's number as xml, json, or protobuf format
#               Sends the value of the square back to the client as xml,
#               json, or protobuf
#
#        PUT /ttt/square
#               The current player moves to the given square, with the data
#               (ie which square to move to) sent in as xml, json, or protobuf
#
#        NEW FOR HOMEWORK 4 to create an API call to interact with Dropbox.
#
#        PUT /ttt/dropbox/name
#               Writes the current board to dropbox under the given name.
#               Basically I'm just writing out the board values in order,
#               e.g. for a game in progress:
#               1 0 2 1 0 0 0 0 0
#
#        GET /ttt/dropbox/name
#               Resets the game to the board saved under this saved name.


import random
import string
import json
import sys
import base64
import binascii
import xml.etree.ElementTree as ET
import time
import os

import dropbox

# See: http://md5decrypt.net/en/Sha256/
# //www.dropbox.com/developers/apps/info/appkey

TOKEN    = 'CscZTFpSs9kAAAAAAAGD8uNohx1sHrcYffI_20KHaC8s29KhOciVocVcV01qy2I-'
FILENAME = '/ttt.txt'
INITIAL  = '0 0 0 0 0 0 0 0 0' # new board

DEBUG    = 0

STATUS_OK         = 0
STATUS_FAILED     = 1
STATUS_DUPLICATE  = 2

TMPFILE = "x.txt"


# Only if I care to support protobufs. Homework #3 did this, but for
# simplicity for this assignment, I'm not requiring this here.
# import square_pb2

PORT = 5000

# Content-Types
type_JSON     = 0
type_XML      = 1
type_PROTOBUF = 2
type_HTML     = 3

content_type = type_HTML # Assume just any regular html-ish thing is fine

from flask import Flask, request

from sense_hat import SenseHat
sense = SenseHat()

num_combos = 8
num_squares = 9

winning_combos = [[1,1,1,0,0,0,0,0,0],
                  [0,0,0,1,1,1,0,0,0],
                  [0,0,0,0,0,0,1,1,1],
                  [1,0,0,1,0,0,1,0,0],
                  [0,1,0,0,1,0,0,1,0],
                  [0,0,1,0,0,1,0,0,1],
                  [1,0,0,0,1,0,0,0,1],
                  [0,0,1,0,1,0,1,0,0]]


# ============================== APIs ====================================

# create the global objects
app = Flask(__name__)

# set up a new game
# newGame()
# newGame() would do what is done here, so I'm just commenting it out for now.
#
# Default to player 1 to move and a fresh board
player = 1
board = [0, 0, 0, 0, 0, 0, 0, 0, 0]
previousMove = 0
sense.clear()


# Login using secure token
def dropbox_login(token):

  dbx = dropbox.Dropbox(token)
  try:
    # Test that we did it:
    info = dbx.users_get_current_account()
    return dbx
  except:
    # No, that didn't work...?
    print "Not a valid token"
    return None

# This helper routine gets the whole file from dropbox into
# memory as the new board.
def dropbox_getfile(dbx, filename):
  try:
    os.remove(TMPFILE)
  except:
    print "No local tmp file for the TTT board, which is fine."

  try:
    dbx.files_download_to_file(TMPFILE, filename)
    f = open(TMPFILE)
    content = f.read()
    f.close()
    return content
  except:
    print "No TTT board file to download from Dropbox, which is fine, carry on."
    return None

# Write a dropbox file containing the contents of the board,
# given entire file contents.
# Returns size of file, or 0 if failed.
def dropbox_putfile(dbx, filename, filecontents):
  try:
    dbx.files_delete(filename)
  except:
    print "No file to delete on Dropbox."
  try:
    dbx.files_alpha_upload(filecontents, filename)
    return len(filecontents)
  except:
    print "Failed to upload to dropbox."
    return 0


# Login to dropbox - do this on starting up the Flask server.
dbx = dropbox_login(TOKEN)

'''
if dbx:
     print("\nLogged into Dropbox")
     filecontents = dropbox_getfile(dbx, FILENAME)
     if not filecontents:
        filecontents = INITIAL
        dropbox_putfile(dbx, FILENAME, filecontents)

     else:
        print "\nFailed to log in to Dropbox!"
        exit(1)
'''



# Make a move
@app.route("/ttt/move/<int:move>", methods=['PUT'])
def TTTMove(move):
  # Move to this square:
  # 1  2  3
  # 4  5  6
  # 7  8  9

  global player
  global board

  # Update the board with this move.
  updateBoard(player, move)

  # Show the move on the sensehat
  pixelx, pixely = mapMoveToPixel(move)
  r, g, b = getPlayerColor(player)
  sense.set_pixel(pixelx, pixely, r, g, b)

  # Let's pause here a moment
  time.sleep(3)

  # Get the game status
  status = checkGameStatus()

  msg = ""

  # Do the right thing.
  if (status == 0):
      # Prep next player to move
      player = changeTurn(player)
      msg = "Game in progress..."

  elif (status == 1):
      msg = "Tic-tac-toe -- red wins!"
      sense.show_message(msg, text_colour=[255, 0, 0])
      time.sleep(1)
      sense.clear()

  elif (status == 2):
      # Player 2 has won!
      msg = "Tic-tac-toe -- blue wins!"
      sense.show_message(msg, text_colour=[0, 0, 255])
      time.sleep(1)
      sense.clear()

  else:
      # Tie!
      msg = "Tie!"
      sense.show_message(msg, text_colour=[255, 255, 255])
      time.sleep(1)
      sense.clear()

  return msg, 200


@app.route("/ttt/whomoves", methods=['GET'])
def TTTWhoMoves():
   global player

   # Figure out content type
   initializeContentType(request.headers)

   status = checkGameStatus()

   msg = ""

   if (status == 0):
      if (player == 1):
          # Player 1 moves
          msg = "Red to move"

      else:
          # Player 2 moves
          msg = "Blue to move"

   else:
       # The game is over
       msg = "Game over"


   return msg, 200

@app.route("/ttt/status", methods=['GET'])
def TTTStatus():
   # Figure out content type
   initializeContentType(request.headers)

   status = checkGameStatus()

   if (status == 0):
      # IN PROGRESS
      msg = "Game in progress"

   elif (status == 1):
      # PLAYER 1 WON
      msg = "Red won"

   elif (status == 2):
       # PLAYER 2 WON
       msg = "Blue won"

   else:
       # TIE
       msg = "Tie"

   # Send a response
   rsp_data = ""

   if (content_type == type_HTML):
     rsp_data = msg

   elif (content_type == type_JSON):
     rsp_data = '{"status":"' + msg + '"}\n'

   elif (content_type == type_XML):
     rsp_data = '<status>' + msg + '</status>\n'

   elif (content_type == "type_PROTOBUF"):
     status = status_pb2.Status()
     status.msg = msg
     mystr = status.SerializeToString()
     rsp_data = base64.b64encode(mystr)

   return rsp_data, 200

@app.route("/ttt/new", methods=['POST'])
def TTTNew():

   # Figure out content type
   initializeContentType(request.headers)

   newGame()

   msg = "New game started"

   # Send a response
   rsp_data = ""

   if (content_type == type_HTML):
     rsp_data = msg

   elif (content_type == type_JSON):
     rsp_data = '{"status":"' + msg + '"}\n'

   elif (content_type == type_XML):
     rsp_data = '<status>' + msg + '</status>\n'

   elif (content_type == "type_PROTOBUF"):
     status = status_pb2.Status()
     status.msg = msg
     mystr = status.SerializeToString()
     rsp_data = base64.b64encode(mystr)

   return rsp_data, 200

@app.route("/ttt/undo", methods=['DELETE'])
def TTTUndo():

    # Figure out content type
    initializeContentType(request.headers)

    undo()

    msg = "Last move deleted"

    # Send a response
    rsp_data = ""

    if (content_type == type_HTML):
      rsp_data = msg

    elif (content_type == type_JSON):
      rsp_data = '{"status":"' + msg + '"}\n'

    elif (content_type == type_XML):
      rsp_data = '<status>' + msg + '</status>\n'

    elif (content_type == "type_PROTOBUF"):
        status = status_pb2.Status()
        status.msg = msg
        mystr = status.SerializeToString()
        rsp_data = base64.b64encode(mystr)

    return rsp_data, 200

# ----------- New API call for Homework #3 ------------------

@app.route("/ttt/square", methods=['GET', 'PUT'])
def TTTSquare():

  global content_type
  global player
  global board

  square = 0

  # Figure out content type
  initializeContentType(request.headers)

  # Either way, GET or PUT, the client sends in the value of the square
  # it's interested in as xml, json, or protobufs format.

  # (For GET, we'll return data as well; but for either case, we have to
  # read the same data -- the number of the tic-tac-toe square to work with.)
  input_valid = False

  # JSON: for example --->   '{"square":1}'
  if content_type == type_JSON:
      data = request.get_json(force=True,silent=True)
      if "square" in data:
        square = int(data["square"])
        input_valid = True

  # XML:  for example --->   '<square>1</square>'
  elif content_type == type_XML:
      data = request.get_data()
      root = ET.fromstring(data)
      if root.tag == 'square':
        square = int(root.text)
        input_valid = True

  # NOTE: I could re-enable this by include square.proto, square.py,
  # square.bin, etc. that's in the lab3 folder for homework 3.
  # base64 'CJYB'
  '''
  elif content_type == type_PROTOBUF:
      data = request.get_data()
      mystr = base64.b64decode(data)
      sq = square_pb2.Square()
      sq.ParseFromString(mystr)
      square = int(sq.number)
      value = int(sq.value)
         # The protobufs structure contains an int for 'value', because it's
         # needed on output. However, here we are just getting the input,
         # which is the square the client is interested in.
         # This int is actually unused as input, because
         # we only need to mark the square indicated for the current player;
         # we might be sending out a value for that square, but we are not
         # _reading_ a value for that square.
      input_valid = True
      '''

  if not input_valid:
    return "Not valid data.\n", 400

  print "---------------------- square = " + str(square)

  value = 0 # default value... it'll be replaced by the real value below.

  if request.method == 'GET':
    # In this case, we'll just give back the value of this
    # square on the board.
    value = board[square - 1] # This is already an int in this array

  if request.method == 'PUT':
      # Make a move to this square for the current player.
      # Update the board with this move.
      updateBoard(player, square) # square is equivalent
                # to 'move' in prev api

      # Show the move on the sensehat
      pixelx, pixely = mapMoveToPixel(square)
      r, g, b = getPlayerColor(player)
      sense.set_pixel(pixelx, pixely, r, g, b)

      # We're not doing any of the game status checking here.
      # This routine is just a demonstration of XML, JSON, and Protobufs
      # support in an API call.
      # However, the game status can easily be checked with the api call
      # /ttt/status

      # The returned value is also in XML, JSON, or
      # Protobufs format.
      # It is the new value of the square after the move.
      value = player

      # Now it's the next player's turn to move.
      player = changeTurn(player)


  if content_type == type_XML:
    # return the value of the square in XML format
    '''
    <?xml version="1.0" encoding="UTF-8"?>
    <board>
      <square>
        <number>1</number>
        <value>1</value>
      </square>
    </board>
    '''

    rsp_data = '<?xml version="1.0" encoding="UTF-8"?>\n'
    rsp_data += '<board>\n'
    rsp_data += '  <square>\n'
    rsp_data += '    <number>' + str(square) + '</number>\n'
    rsp_data += '    <value>' + str(value) + '</value>\n'
    rsp_data += '  </square>\n'
    rsp_data += '</board>'

    # Could re-enable as per the note above about protobufs.
    '''
    elif content_type == type_PROTOBUF:
        # return the value of the square in Google Protobuf format
        sq = square_pb2.Square()
        sq.number = square
        sq.value = value
        s = sq.SerializeToString()
        rsp_data = base64.b64encode(s)
    '''


  else:
    # return the value of the square in JSON format
    '''
    {"board":{"square":1,"value":1}}
    '''

    rsp_data = '{"board":'
    rsp_data += '{"square":' + str(square) + ',"value":' + str(value) + '}'
    rsp_data += '}\n'

  return rsp_data, 200

# ----------- New API call for Homework #4 ------------------

@app.route("/ttt/dropbox/save", methods=['POST'])
def TTTDropboxSave():

  global player
  global board
  global dbx

  # Saves or retrieves a game board and who's turn it is.

  # app folder name:    bb-ttt
  # acccess:            only me
  # app key:            yptlwc2am4lcn6h
  # app secret:         petrn65di10ml73
  # status:             development
  # acct:               bmb@g
  # access token:       CscZTFpSs9kAAAAAAAGD8uNohx1sHrcYffI_20KHaC8s29KhOciVocVcV01qy2I-

  # pip install dropbox

  # This worked:
  '''
  $ python
  >>> import dropbox
  >>> dbx = dropbox.Dropbox("YOUR_ACCESS_TOKEN")
  >>> dbx.users_get_current_account()
  '''

  filecontents = ''
  for n in board:
      filecontents += str(n) + ' '

  if dropbox_putfile(dbx, '/ttt.txt', filecontents) == 0:
      print "Something went wrong, could not save the board to Dropbox."
  else:
      print "Board saved in Dropbox file."

  return "OK", 200

@app.route("/ttt/dropbox/retrieve", methods=['PUT'])
def TTTDropboxRetrieve():

    global player
    global board
    global dbx


    # Reset the game...
    newGame()

    filecontents = dropbox_getfile(dbx, '/ttt.txt')

    if (filecontents == None):
      print "Something went wrong, could not retrieve the board from Dropbox."
    else:
      print "Board retrieved from Dropbox file."

      # Parse the file and initialize the board.
      # Great line of code from here:
      # https://stackoverflow.com/questions/4289331/python-extract-numbers-from-a-string
      board = [int(s) for s in filecontents.split() if s.isdigit()]

    # ...and update the board with the new display
    x = 0
    for n in board:
      x += 1

      if (n > 0):
          pixelx, pixely = mapMoveToPixel(x)
          r, g, b = getPlayerColor(n)
          sense.set_pixel(pixelx, pixely, r, g, b)


    # Set who's turn it is.
    player = checkWhosTurn()

    return "OK", 200



########################################
# Helper functions
########################################

def newGame():
   global player
   global board
   global previousMove

   player = 1
   board = [0, 0, 0, 0, 0, 0, 0, 0, 0]
   sense.clear()
   previousMove = 0

def updateBoard(p, m):
  global board
  global previousMove

  # move player x to square m
  board[m - 1] = p
  previousMove = m

def undo():
  global previousMove
  global player

  # Only 1 level deep -- implemented to show an example of DELETE in the API.
  updateBoard(0, previousMove)
  player = changeTurn(player)

  # Turn off that pixel
  x, y = mapMoveToPixel(previousMove)
  sense.set_pixel(x, y, 0, 0, 0)

def checkGameStatus():
   global num_combos
   global num_squares
   global winning_combos
   global board

   # 0 = in progress still
   # 1 = player 1 won
   # 2 = player 2 won
   # 3 = tie, game over
   status = 0

   # First check player 1 for winning combo
   for combo in xrange(0, num_combos):
       ttt = 0
       for square in xrange(0, num_squares):
          if (winning_combos[combo][square] > 0 and board[square] == 1):
             ttt += 1
          if (ttt == 3):
              # tic-tac-toe, 3 in a row!
              status = 1

   # Then check player 2 for winning combo if status is still 0
   if (status == 0):
      for combo in xrange(0, num_combos):
         ttt = 0
         for square in xrange(0, num_squares):
            if (winning_combos[combo][square] > 0 and board[square] == 2):
               ttt += 1
            if (ttt == 3):
                # tic-tac-toe, 3 in a row!
                status = 2

   # Then check for tie if status is still 0
   if (status == 0):
      if (0 not in board):
          # All positions are filled in the board
          status = 3

   return status

def checkWhosTurn():
    # From the number of 1's and 2's, figure out who's turn it is.
    # If there are equal numbers of 1's and 2's (including 0 of each),
    # then it is player 1's turn.
    # If there 1's > 2's, it is player 2's turn.
    global board
    global player

    p1 = 0
    p2 = 0

    for n in board:
        if (n == 1):
            p1 += 1
        elif (n == 2):
            p2 += 1

    if (p1 > p2):
        player = 2

    else:
        player = 1


def changeTurn(p):
    if (p == 1):
        return 2
    else:
        return 1

def getPlayerColor(p):
    if (p == 1):
        return 255, 0, 0 # player 1 is red
    else:
        return 0, 0, 255 # player 2 is blue

def mapMoveToPixel(move):
   if (move == 1):
       return 1, 1
   elif (move == 2):
       return 4, 1
   elif (move == 3):
       return 7, 1
   elif (move == 4):
       return 1, 4
   elif (move == 5):
       return 4, 4
   elif (move == 6):
       return 7, 4
   elif (move == 7):
       return 1, 7
   elif (move == 8):
       return 4, 7
   else:
       return 7, 7

# Routine to figure out the content type and set globals
def initializeContentType(rh):
  global content_type

  content_json      = 'application/json'
  content_xml       = 'application/xml'
  content_protobufs = 'application/protobuf'

  # assume JSON
  content_type = type_HTML

  if ('Content-Type' in rh):
      if (rh['Content-Type'] == content_xml):
          content_type = type_XML
      elif (rh['Content-Type'] == content_protobufs):
          content_type = type_PROTOBUF
      elif (rh['Content-Type'] == content_json):
          content_type = type_JSON


# ============================== Main ====================================

if __name__ == "__main__":

  print("HTTP Server")

  app.debug = True
  app.run(host='0.0.0.0', port=PORT)
