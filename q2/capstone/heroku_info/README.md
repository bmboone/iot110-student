# Capstone Project: Entangled scarves

I created a prototype wearable electronics product for this
capstone project. Lightweight electronics are sewn onto two
pieces of fabric and are meant to be sewn onto a scarf and worn
around your neck. The two scarves are "entangled" and can
communicate with each other, almost anywhere in the world.

I actually created this project as a (late) Valentine's day present
for my wife and me.

When either person wearing one of the entanlged scarves
touches a special square of fabric on their scarf, a small LED
lights up on their scarf to show they have activated it. The
entangled scarf, no matter where it is, turns on one of its own
lights, to show the other person is thinking of them.

If the second person touches the special square of fabric on their
scarf within 5 minutes, both scarves turn their lights to red. The
red lights stay on for 5 minutes before turning off.

Interactions are recorded in a postgres database running in the cloud
(on a Heroku server) to keep track of scarf events.

# The magic under the hood

The scarves communicate via 2G, using GPRS (General Packet Radio Service) for reporting if one of them has turned on. The Heroku server,
running as a Flask app, uses a Heroku service to send an SMS message
to the entangled scarf as appropriate. The scarf receives this message
via an Adafruit Flora (basically, a sewable Arduino) and does the
correct thing with its LED lights.

# Further references

See the two jpg files "entangled-physical-layout" and "entangled-system" for diagrams.
