from flask import *
from flask_sqlalchemy import SQLAlchemy
import os
import threading # For timer
from datetime import datetime
import requests



# Barry Boone
# 2/27/18
# This code is for the Entangled Scarves wearable IoT communicator project.
# This is for IoT Capstone #2.
# It connects two scarves so that they are "entangled." When the owner
# of one scarf touches a capacitive fabric in the scarf, it sends a message
# to the other scarf that the owner is thinking of them. If the entangled
# scarf's owner responds with a touch, the lights on both scarves turn
# colorful.

'''
Two things can happen on the Heroku side:

1. A Flora-Fona combo hits the URL to report that its attached
   scarf has turned on

2. A timer running on the server times out


Event #1: Heroku URL gets a POST for scarf 1 indicating it turned ON

- log POSTED ON in db for scarf 1

- if scarf 2 is STATUS_ON or STATUS_BOTH_ON:
  - set timer (log)
  - SMS to scarf 1; STATUS_BOTH_ON (log)
  - SMS to scarf 2: STATUS_BOTH_ON (log)
  - set status of scarf 1: STATUS_BOTH_ON (log)
  - set status of scarf 2: STATUS_BOTH_ON (log)

- if scarf 2 is STATUS_WAITING or STATUS_BOTH_ON:
  - set timer (log)
  - SMS to scarf 2: STATUS_BOTH_ON (log)
  - set status of scarf 1: STATUS_ON (log)
  - set status of scarf 2: STATUS_BOTH_ON (log)


NOTE: An alternate approach is to keep the timer only locally on the Flora,
and on timeout, they hit a URL on this server, which then knows to update
its records here in the central db.

Event #2: Timer hits zero
(Note: There is also a timer on each scarf's Flora, so no need to SMS
them from here, as they're keeping track of this locally.)

- set status of scarf 1 to STATUS_WAITING (log)

- set status of scarf 2 to STATUS_WAITING (log)

'''

app = Flask(__name__)


# Heroku sets the environment variable DATABASE_URL,
# automatically, all I have to do is read it in here via os.environ.
# According to heroku, the password and event the user could change
# if they recycle the environment for my app,
# so if I set it myself it could change on me.
# On the mac, I expect this name myself, and of course it will stay
# the same on my mac unless I change it.

# Here is the command I used to set this info on my mac before
# running the app:
# export DATABASE_URL="postgresql://postgres:curio@localhost:5432/entangled2"


uri = os.environ['DATABASE_URL']

# Setting this config variable helps make the SQLAlchemy magic
# work for the ORM.
app.config['SQLALCHEMY_DATABASE_URI'] = uri

# All I have to do is ask for the database and here it is!
db = SQLAlchemy(app)

# If I were going to have more than 1 entangled set of scarves, I would
# keep timers in a db, or array, or something. But I only am going to have
# 1 entangled scarf for the foreseeable future so this is left to be done
# later if I ever want to do that.
currentTimer = None

# TODO: Separate out the ORM models so it's not all defined
# here in main.py.
# from models import db and all ORMs

class StatusTypes(db.Model):
    # __tablename__ = 'status_types'

    id = db.Column(db.Integer, primary_key=True)
    abbreviation = db.Column(db.String, unique=True, nullable=False)
    description = db.Column(db.String, nullable=False)

class EventTypes(db.Model):
    # __tablename__ = 'event_types'

    id = db.Column(db.Integer, primary_key=True)
    abbreviation = db.Column(db.String, unique=True, nullable=False)
    description = db.Column(db.String, nullable=False)

class Scarves(db.Model):
    # __tablename__ = 'scarves'

    id = db.Column(db.Integer, primary_key=True)
    status_type_id = db.Column(db.Integer,
            db.ForeignKey("status_types.id"), nullable=False)
    sms_info = db.Column(db.String, nullable=False)
    sim_number = db.Column(db.String, nullable=True)
    owner = db.Column(db.String, nullable=False)

    # A scarf can have many events, but I don't currently need to access
    # this relationship.
    #
    # events = db.relationship('Events',
    #            backref='scarf', lazy=True)

    '''
    scarf class:

    methods:
    - getEntangledScarf()
      - find and return the entangled scarf via the db

    - sendSMS(msg)
      - Send the SMS message to this scarf
      - log this to the event log

    - setStatus(status_abbr)
      - set the status in the object
      - set this in the db

    - logEvent(event_abbr)
      - update the event log db

    - getStatusAbbr()

    '''

    def getEntangledScarf(self):
        entanglement = Entanglements.query.filter_by(scarf1_id=self.id)

        # We're just going to ensure for now there's just one record for
        # this entanglement and so we just choose the first record.
        # But in theory, we could have multiple entangled scarves.
        entangledScarf = Scarves.query.get(entanglement[0].scarf2_id)
        return entangledScarf

    def sendSMS(self, msg):
        requests.post(os.environ['BLOWERIO_URL'] + '/messages',
                data={'to': self.sms_info,
                'message': msg})

    def setStatus(self, status_abbr):
        status_type = StatusTypes.query.filter_by(abbreviation = status_abbr)
        self.status_type_id = status_type[0].id

        dbsession = db.session;
        dbsession.commit()
        # dbsession.flush()

        # Log this event
        self.logEvent(status_abbr)

    def logEvent(self, event_abbr):
        event_type = EventTypes.query.filter_by(abbreviation = event_abbr)

        event = Events(scarf_id = self.id,
                    event_type_id = event_type[0].id)

        dbsession = db.session;
        dbsession.add(event)
        dbsession.commit()
        # dbsession.flush()

    def getStatusAbbr(self):
        statusType = StatusTypes.query.get(self.status_type_id)
        return statusType.abbreviation



class Entanglements(db.Model):
    # __tablename__ = 'entanglements'

    id = db.Column(db.Integer, primary_key=True)
    scarf1_id = db.Column(db.Integer,
        db.ForeignKey("scarves.id"), nullable=False)
    scarf2_id = db.Column(db.Integer,
        db.ForeignKey("scarves.id"), nullable=False)

class Events(db.Model):
    # __tablename__ = 'events'

    id = db.Column(db.Integer, primary_key=True)
    scarf_id = db.Column(db.Integer,
        db.ForeignKey("scarves.id"), nullable=False)
    event_type_id = db.Column(db.Integer,
        db.ForeignKey("event_types.id"), nullable=False)
    event_time = db.Column(db.DateTime, nullable=False,
        default=datetime.utcnow)

# Uses threading, so non-blocking to set up the timer.
def startTimer(scarf1_id, scarf2_id):

    global currentTimer

    # First I want to cancel the current timer, if there is one:
    if (currentTimer is not None):
        currentTimer.cancel()

    # Timer times out in 5 minutes.
    currentTimer = threading.Timer(300, timedOut, args = [scarf1_id, scarf2_id])
    currentTimer.start()

def timedOut(scarf1_id, scarf2_id):

    global currentTimer

    # I want to reset the status for scarf1 and scarf2 on timeout
    scarf1 = getScarfFromId(scarf1_id)
    scarf2 = getScarfFromId(scarf2_id)

    scarf1.setStatus("STATUS_WAITING")
    scarf2.setStatus("STATUS_WAITING")

    # Now I want to set currentTimer to None because I have no current
    # timer running.
    currentTimer = None


# Helper to get the scarf object given its id
def getScarfFromId(scarf_id):
  scarf = Scarves.query.get(scarf_id)
  return scarf

def getScarfFromSimNumber(simn):
    try:
        scarf = Scarves.query.filter_by(sim_number = simn).first()
    except:
        scarf = None

    return scarf


@app.route("/")
def index():
    return 'You have reached the Entangled Scarves server.'

# This URL is for POSTing that the owner of a particular scarf has just
# turned on their scarf.
@app.route("/scarf/on/<string:sim_number>")
def scarf_on(sim_number):

  # Get this scarf object.
  scarf = getScarfFromSimNumber(sim_number)

  # log that this scarf initiated a POSTED ON call to this url
  scarf.logEvent(event_abbr = "POSTED_ON")

  # Find which scarf this one is entangled with.
  entangledScarf = scarf.getEntangledScarf()

  entangled_status_abbr = entangledScarf.getStatusAbbr()

  # if entangled scarf is set to ON or STATUS_BOTH_ON:
  if (entangled_status_abbr == "STATUS_ON" or entangled_status_abbr == "STATUS_BOTH_ON"):

    # If we're here, both scarves are on now.

    # set timer (log)
    # When this timer times out, turn off both scarves' lights
    startTimer(scarf.id, entangledScarf.id)

    # set status of scarf 1: STATUS_BOTH_ON (log)
    scarf.setStatus(status_abbr = "STATUS_BOTH_ON")

    # set status of scarf 2: STATUS_BOTH_ON (log)
    entangledScarf.setStatus(status_abbr = "STATUS_BOTH_ON")

    # SMS to scarf 2: STATUS_BOTH_ON (log)
    entangledScarf.sendSMS(msg = "STATUS_BOTH_ON")

    # SMS to scarf 1; STATUS_BOTH_ON (log)
    scarf.sendSMS(msg = "STATUS_BOTH_ON")

    # Log this event
    scarf.logEvent("SMS_BOTH_ON")

    # Log this event
    entangledScarf.logEvent("SMS_BOTH_ON")

  # if scarf 2 is STATUS_WAITING or STATUS_ENTANGLED_ON:
  else:

    # If we're here, only scarf 1 is on, and scarf 2 is not yet on.

    # set timer (log) only if we're in the status is STATUS_WAITING.
    # When this timer times out, turn off scarf 1's light
    startTimer(scarf.id, entangledScarf.id)

    # SMS to scarf 2: STATUS_BOTH_ON (log)
    entangledScarf.sendSMS(msg = "STATUS_ENTANGLED_ON")

    # log
    entangledScarf.logEvent("SMS_ENTANGLED_ON")

    # set status of scarf 1: STATUS_ON (log)
    scarf.setStatus(status_abbr = "STATUS_ON")

    # set status of scarf 2: STATUS_ENTANGLED_ON (log)
    entangledScarf.setStatus(status_abbr = "STATUS_ENTANGLED_ON")

  return 'OK'


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, threaded=True)
