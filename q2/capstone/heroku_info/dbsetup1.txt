


CREATE TABLE status_types(
  id SERIAL PRIMARY KEY NOT NULL,
  abbreviation TEXT NOT NULL,
  description TEXT NOT NULL
);

INSERT INTO status_types (abbreviation, description)
VALUES
 ('STATUS_WAITING', 'This scarf is off and its entangled scarf is off.'),
 ('STATUS_ON', 'This scarf is on but its entangled scarf is off.'),
 ('STATUS_ENTANGLED_ON', 'This scarf is off, but it has been notified that its entangled scarf is on.'),
 ('STATUS_BOTH_ON', 'Both this scarf and its entangled scarf are on.');


 CREATE TABLE event_types(
   id SERIAL PRIMARY KEY NOT NULL,
   abbreviation TEXT NOT NULL,
   description TEXT NOT NULL
 );

 INSERT INTO event_types (abbreviation, description)
 VALUES
  ('POSTED_ON', 'Scarf POSTED: ON, via URL.'),
  ('STATUS_WAITING', 'Set status to WAITING.'),
  ('STATUS_ON', 'Set status to ON.'),
  ('STATUS_ENTANGLED_ON', 'Set status to ENTANGLED ON.'),
  ('STATUS_BOTH_ON', 'Set status to BOTH ON.'),
  ('SMS_ENTANGLED_ON', 'Sent SMS: ENTANGLED ON.'),
  ('SMS_BOTH_ON', 'Sent SMS: BOTH ON.');


CREATE TABLE scarves(
  id SERIAL PRIMARY KEY NOT NULL,
  status_type_id INTEGER NOT NULL REFERENCES status_types(id),
  sms_info TEXT NOT NULL,
  sim_number TEXT,
  owner TEXT NOT NULL
);

// Need to insert the sim numbers and phone numbers when I get them.

status_id =
SELECT id FROM status_types WHERE abbreviation = 'WAITING';



CREATE TABLE entanglements(
  id SERIAL PRIMARY KEY NOT NULL,
  scarf1_id INTEGER NOT NULL REFERENCES scarves(id),
  scarf2_id INTEGER NOT NULL REFERENCES scarves(id)
);

scarf1_id =
SELECT id FROM scarves WHERE owner = 'Barry';

scarf2_id =
SELECT id FROM scarves WHERE owner = 'Mary';

INSERT INTO entanglements (scarf1_id, scarf2_id)
VALUES
 (1, 2),
 (2, 1);




CREATE TABLE events(
  id SERIAL PRIMARY KEY NOT NULL,
  scarf_id INTEGER NOT NULL REFERENCES scarves(id),
  event_type_id INTEGER NOT NULL REFERENCES event_types(id),
  event_time TIMESTAMP NOT NULL
);
