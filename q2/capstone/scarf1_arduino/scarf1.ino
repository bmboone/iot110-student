 /*
 * Entangled scarf code for Adafruit's Flora
 * Barry Boone 2018
 */

// No longer necessary: Now goes off SIM card number, which must be in
// the database on the server, so all Flora code can be identical now.
// int scarfNumber = 1; // This number must be set for each scarf.

bool debug_on = true;
bool test_lights_on_touch = true;
 
#include <CapacitiveSensor.h>
#include <Adafruit_NeoPixel.h>
#include "Adafruit_FONA.h"
#include <stdio.h>

#define FONA_RX 10
#define FONA_TX 9
#define FONA_RST 6 // was 12

// We default to using software serial. If you want to use hardware serial
// (because softserial isnt supported) comment out the following three lines 
// and uncomment the HardwareSerial line
#include <SoftwareSerial.h>
SoftwareSerial fonaSS = SoftwareSerial(FONA_TX, FONA_RX);
SoftwareSerial *fonaSerial = &fonaSS;

#define NEOPIXEL_PIN 12  // Neopixel // was 6
#define CAP1_PIN 0      // One side of the capacitive touch fabric
#define CAP2_PIN 1      // The other side of the capacitive touch fabric
// Also worked using pins (9,10) but I needed those for the Fona



// Trying 2 and 2
CapacitiveSensor   capSensor = CapacitiveSensor(CAP1_PIN,CAP2_PIN);      // I have a 100k res between pins 9 and 10
    // Just noting there's a 100K resistor between pins CAP1 and CAP2

Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, NEOPIXEL_PIN, NEO_GRB + NEO_KHZ800);

// this is a large buffer for replies
char replybuffer[255];

// to hold the sim number
char sim_number[25]; // plenty big enough, sim numbers are currently 19 chars

// Use this for FONA 800 and 808s
Adafruit_FONA fona = Adafruit_FONA(FONA_RST);

uint8_t type;

#define STATUS_WAITING        0
#define STATUS_ON             1
#define STATUS_ENTANGLED_ON   2
#define STATUS_BOTH_ON        3

int scarfStatus = STATUS_WAITING;

#define LIGHT_OFF     0
#define LIGHT_WHITE   1
#define LIGHT_COLORS  2

uint32_t timerTime = 0;



bool timerSet = false;


int currentTimesThrough = 0;
int THRESHOLD_TIMES_THROUGH = 10;

void setTimer() {
  timerTime = millis();
  timerSet = true;
}

// 300000 below is 300 seconds (5 minutes) * 1000 milliseconds/second
bool didTimerRunOut() {
  if (timerSet) {
    if ((millis() - timerTime) > 300000) {

      if (debug_on) {
        Serial.println("Timer ran out.");
      }
      return true;
    }
  }

  return false;
}

void setLight(int whichLight, int lightColor) {
  switch (lightColor) {
    case LIGHT_OFF:
      strip.setPixelColor(whichLight - 1, strip.Color(0, 0, 0));   // lights are 0-based in neopixel chain
      strip.show();
      break;

    case LIGHT_WHITE:
      strip.setPixelColor(whichLight - 1, strip.Color(255, 255, 255));  
      strip.show();
      break;

    // I'm just setting the light to red for COLORS for now. 
    case LIGHT_COLORS:
      strip.setPixelColor(whichLight - 1, strip.Color(255, 0, 0));  
      strip.show();
      break;

    default:
      break;
  }
}

void statusLightsTimer(int status) {
  scarfStatus = status;
                
  // update lights
  updateLights();
                
  // set timer
  setTimer();
}

// Seems like I'm getting one of two phone numbers from blower.io, but this test is not
// critical for demonstrating this capstone project.
bool trustMessage(char * s) {

  if (debug_on) {
    Serial.print("heroku phone number: ");
    Serial.print("\t");
    Serial.println(s);
  }
  return true;

  /*
  bool r = false;
  char * herokuPhoneNumber = "+13158163021";
  r = (strcmp(s, herokuPhoneNumber) == 0);

  if (!r) {
    herokuPhoneNumber = "+12063130057";
    r = (strcmp(s, herokuPhoneNumber) == 0);
  }

  return r;
  */
}


/*
 * 
 * 
    State         Light 1         Light 2
    WAITING       off             off
    ON            white           off
    ENTANGLED ON  off             white
    BOTH ON       cycle colors    cycle colors
 
 *
 */
 
void updateLights() {
    switch (scarfStatus) {
      case STATUS_WAITING:
        setLight(1, LIGHT_OFF);
        setLight(2, LIGHT_OFF);
        break;

      case STATUS_ON:
        setLight(1, LIGHT_WHITE);
        setLight(2, LIGHT_OFF);
        break;

      case STATUS_ENTANGLED_ON:
        setLight(1, LIGHT_OFF);
        setLight(2, LIGHT_WHITE);
        break;

      case STATUS_BOTH_ON:
        setLight(1, LIGHT_COLORS);
        setLight(2, LIGHT_COLORS);
        break;

      default:
        break;
    }
}

// Helpful function from stackoverflow:
char *trimwhitespace(char *str)
{
  char *end;

  // Trim leading space
  while(isspace((unsigned char)*str)) str++;

  if(*str == 0)  // All spaces?
    return str;

  // Trim trailing space
  end = str + strlen(str) - 1;
  while(end > str && isspace((unsigned char)*end)) end--;

  // Write new null terminator
  *(end+1) = 0;

  return str;
}

void setup()                    
{
  //capSensor.set_CS_AutocaL_Millis(0xFFFFFFFF);     // turn off autocalibrate on channel 1 - just as an example
  Serial.begin(115200);

  // Let's update the lights to their initial waiting state.
  updateLights();
  
  strip.begin();
  strip.show();

  while (!Serial);

  Serial.begin(115200);

  fonaSerial->begin(4800);
  if (! fona.begin(*fonaSerial)) {
    Serial.println(F("Couldn't find Fona"));
    while (1);
  }

  if (debug_on) {
    Serial.println("Found fona!");
  }

  // Get the sim card number to use when hitting the url that this scarf turned on.
  fona.getSIMCCID(sim_number);
}

void hitURL() {
   // Turn on GPRS:
   fona.enableGPRS(true);

   // Hit my heroku server so it knows this scarf is on 
   uint16_t statuscode;
   int16_t resplength;
   char url[80] = "http://entangled2.herokuapp.com/scarf/on/"; // Add on the scarf number!

   // Set the url to:
   // "http://entangled2.herokuapp.com/scarf/on/" + sim_number // scarfNumber

   //char s[5];
   //itoa(scarfNumber, s, 10); // 10 means base 10
   // Used to cat s onto the url, now we cat the sim number and send that, 
   // this way all flora code can be the same.

   strcat(url, sim_number);
         
   fona.HTTP_GET_start(url, &statuscode, (uint16_t *)&resplength);
   
   // Response not used.
   fona.HTTP_GET_end();

   // Turn off GPRS:
   fona.enableGPRS(false);
}

void processSMS() {
  char * s;
  
  // read all SMSs
  int8_t    smsnum = fona.getNumSMS();
  uint16_t  smslen;
  int8_t    smsn;

  if ( (type == FONA3G_A) || (type == FONA3G_E) ) {
    smsn = 0; // zero indexed
    smsnum--;
  } else {
    smsn = 1;  // 1 indexed
  }

  for ( ; smsn <= smsnum; smsn++) {

    if (debug_on) {
      Serial.print("Reading SMS #"); 
      Serial.println(smsn);
    }

    // First, let's check out the phone number in this sms and make sure it's from my server.
    fona.getSMSSender(smsn, replybuffer, 250);

    if (debug_on) {
      Serial.print("replybuffer trimmed value is (should be phone number): ");
      Serial.println(s);
    }
       
    if (trustMessage(s)) {

      if (debug_on) {
        Serial.println("Trusting message");
      }
   
          
      if (!fona.readSMS(smsn, replybuffer, 250, &smslen)) {  // pass in buffer and max len!
        Serial.println("Failed!");
        break;
      }
          
      // if the length is zero, its a special case where the index number is higher
      // so increase the max we'll look at!
      if (smslen == 0) {
        Serial.println("empty slot");
        smsnum++;
        continue;
      }

      // Process what's in the replybuffer

      if (debug_on) {
        Serial.print("replybuffer is (body of sms): ");
        Serial.println(replybuffer);
      }

      // Check that what's in replybuffer is equal to the phone number I expect:
      s = trimwhitespace(replybuffer);

      // We're going to receive one of two messages:
      // "STATUS_BOTH_ON"
      // "STATUS_ENTANGLED_ON"

      if (strcmp(s, "STATUS_BOTH_ON") == 0) {

        statusLightsTimer(STATUS_BOTH_ON);

      } else if (strcmp(s, "STATUS_ENTANGLED_ON") == 0) {

        // if internal state is ON or BOTH ON:
        if (scarfStatus == STATUS_ON || scarfStatus == STATUS_BOTH_ON) {

          statusLightsTimer(STATUS_BOTH_ON);

        // else if internal state is WAITING or ENTANGLED ON:
        } else if (scarfStatus == STATUS_WAITING || scarfStatus == STATUS_ENTANGLED_ON) {

          statusLightsTimer(STATUS_ENTANGLED_ON);

        } // status_waiting or status_entangled_on
      } // entanged_on
    } // trust message

    // Let's delete this SMS so it doesn't just sit around on the SIM card.
    fona.deleteSMS(smsn);
    
  } // Loop through all the sms messages
}
        



void loop()                    
{


    // Any of these three things can happen:
    // 1. Get a capacitive touch
    // 2. Receive an SMS (via Fona)
    // 3. A timer times out

    // Start with #1: Did the user touch the capacitive fabric?
    long capTotal =  capSensor.capacitiveSensor(30);

    if (debug_on) {
      Serial.print("capTotal is: " );
      Serial.println(capTotal);
    }

    // Discovered empirically what the # should be here.
    if (capTotal > 50) {

        if (debug_on) {
          Serial.println("Capacitive touch detected!");
        }


        if (!test_lights_on_touch) {
          // If internal state: WAITING or ON:
          if (scarfStatus == STATUS_WAITING || scarfStatus == STATUS_ON) {
  
              statusLightsTimer(STATUS_ON);
              
              // Via Fona, hit the URL so the entangled server knows this scarf is now ON
              hitURL();
  
          // if internal state: ENTANGLED ON or BOTH ON:
          } else if (scarfStatus == STATUS_ENTANGLED_ON || scarfStatus == STATUS_BOTH_ON) {
   
            statusLightsTimer(STATUS_BOTH_ON);
            
            // Via Fona, hit the URL so the entangled server knows this scarf is now ON
            hitURL();
          }
        }

        if (test_lights_on_touch) {
          setLight(1, LIGHT_WHITE);
          setLight(2, LIGHT_COLORS); 
        }      
        
    } // we got a capacitive touch 

    // We don't need to _constantly_ check the fona -- let's check every 10 times through.
    currentTimesThrough++;
    if (currentTimesThrough > THRESHOLD_TIMES_THROUGH)  {

       currentTimesThrough = 0;
     
       // Now let's check for the second thing that can happen in this loop: Is there an SMS waiting to be read?
       int8_t numSMSs = fona.getNumSMS();

       if (numSMSs > 0) {

          if (debug_on) {
            Serial.print("Found an sms -- we have this many: ");
            Serial.println(numSMSs);
          }
          // Hey! We received an SMS.
          processSMS();
          
       } // there's an sms to process
    } // we are looking for an sms because enough time has elapsed to make this worthwhile
       
         
    // The #3 thing that could happen is: Did the timer run out?
    // If timer runs out on Flora:
    if (timerSet) {
      if (didTimerRunOut()) {

        timerSet = false;
        // set state to WAITING
        scarfStatus = STATUS_WAITING;
       
        // update lights
        updateLights();
      }
    } // timer
 
    delay(1500);    // Arbitrary delay to limit data to serial port, no need to loop more than once every second or two
                    // This also sort of acts as a debouncer, too, since the user has a moment to remove their touch.

} // ...and that's the end of the loop method




