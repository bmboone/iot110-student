#!/usr/bin/python
# =============================================================================
#        File : addr_plus_temp_color.py
# Description : Displays IP addr on the Raspberry Pi SenseHat 8x8 LED display
#               e.g. '10.193.72.242'
#               THEN, it displays a color range, red to blue, showing the
#               temperature, ranging from 60 to 100 degrees (indoor temp)
#      Author : Barry Boone, based on Drew Gislsason's code for Lab1
#        Date : 1/7/2018
# =============================================================================

# To use on bootup of :
# sudo nano /etc/rc.local add line: python /home/pi/ipaddr.py &
#

from sense_hat import SenseHat
sense = SenseHat()
sense.set_rotation(180)
sense.show_letter('P')

# give time to get on the Wi-Fi network
import time
time.sleep(10)

# get our local IP address. Requires internet access (to talk to gmail.com).
import socket

# A helper routine for later:
# Go from one range to another.
def mytranslate(value, leftMin, leftMax, rightMin, rightMax):
    # Figure out how 'wide' each range is
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin

    # Convert the left range into a 0-1 range (float)
    valueScaled = (value - leftMin) / (leftSpan)

    # Convert the 0-1 range into a value in the right range.
    return rightMin + (valueScaled * rightSpan)


s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("gmail.com",80))
addr = s.getsockname()[0]
s.close()

# display that address on the sense had
sense.show_message(str(addr))

# Let's build in a short delay.
time.sleep(3)

##########################################
# Now let's show the temperature as a range from blue to red
##########################################

# Get the temperature
t = sense.get_temperature()

# Adjust to my range -- let's put it between 60 and 100,
# as this will be an indoors value
t = round(t, 1)

if (t < 60):
   t = 60

if (t > 100):
   t = 100

# Decide on the colors: blue = cold, mix in-between, red = hot
blue_amt = mytranslate(t, 60, 100, 255, 0) # The colder, the bluer
red_amt = mytranslate(t, 60, 100, 0, 255)  # The hotter, the redder

X = [red_amt, 0, blue_amt]

# Now let's turn on pixels to show this color:
# (I'm currently turning them all on.)
pixels_to_turn_on = [
X, X, X, X, X, X, X, X,
X, X, X, X, X, X, X, X,
X, X, X, X, X, X, X, X,
X, X, X, X, X, X, X, X,
X, X, X, X, X, X, X, X,
X, X, X, X, X, X, X, X,
X, X, X, X, X, X, X, X,
X, X, X, X, X, X, X, X
]

sense.set_pixels(pixels_to_turn_on)

time.sleep(5)
sense.clear() # Clear the display
