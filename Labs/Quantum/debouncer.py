SHIFT_MASK = 15
class Debouncer(object):
    """Shift Register Debouncer"""

    def __init__(self):
        self.sensor_shift_register = 0

    # perform AND logic debouncer
    def debounce(self, switch_in):
        self.sensor_shift_register = (self.sensor_shift_register << 1) | switch_in
        return int((self.sensor_shift_register & SHIFT_MASK) == SHIFT_MASK)
