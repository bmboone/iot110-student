import RPi.GPIO as GPIO
import time

LIGHT_SENSOR = 23

class PiGpio(object):
    """Raspberry Pi Internet 'IQoT GPIO'."""

    def __init__(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(LIGHT_SENSOR, GPIO.OUT)   #
        GPIO.output(LIGHT_SENSOR, GPIO.HIGH) # Start as output to set high -- light detecting!

        # Now we're going to use it as input.
        GPIO.setup(LIGHT_SENSOR, GPIO.IN)


    def read_light_sensor(self):
        """Read the light sensor."""

        light_sensor = GPIO.input(LIGHT_SENSOR)

        # High resistance, no light
        # Low resistance, light
        if (light_sensor == GPIO.LOW):
            light_detected = 0
        else:
            light_detected = 1

        #print("light detected:")
        # print(light_detected)
        return light_detected
