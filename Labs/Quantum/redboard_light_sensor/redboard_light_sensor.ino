

/////////////////////////////////////////////////////////////////////
//set the pins
/////////////////////////////////////////////////////////////////////
const int LIGHT_DETECTOR = A0;
const int THRESHHOLD = 300;    // based on empirical testing -- 
                               // higher = light detected, lower = no light


int sensorValue = 0;            // value read from light sensor
int prevLightDetected = false;

void setup() {
  Serial.begin(9600);      
  pinMode(LIGHT_DETECTOR,  INPUT); 

  sensorValue = analogRead(LIGHT_DETECTOR);
  if (sensorValue > THRESHHOLD) {
    prevLightDetected = true;
  } else {
    prevLightDetected = false;
    
  }
}

void loop() {

  sensorValue = analogRead(LIGHT_DETECTOR);
  
  // Send this sensor data to the Mac or Pi if it changed to above or below a threshhold.

  if (sensorValue > THRESHHOLD) {  // We are detecting light!
    if (prevLightDetected) {
      // Don't do anything, we're still detecting light, same as the last time through.
    } else {
      prevLightDetected = true;

      // Things have changed!
      Serial.println(1);
    }
    
  } else {  // We are NOT detecting light
    if (prevLightDetected) {
      prevLightDetected = false;

      // Things have changed!
      Serial.println(0);

    } else {
      // Don't do anything, we're still *not* detecting light, same as last time.
    }
      
  }

  delay(1000); // short delay between loop cycles
}








