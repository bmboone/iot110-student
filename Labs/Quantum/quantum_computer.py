# import urllib.request
# This is for python 3

# Python 2 version:
import urllib2
# urllib2.urlopen("http://qcat-server.herokuapp.com/run_superposition/1024").read()

class QuantumComputer(object):
    """For the Raspberry Pi 'IQoT' Project."""

    def runSuperposition(self, nShots):
      # This will take a little while to run -- it hits the end point I've set up on Heroku
      # to act as a gateway to the quantum computer on the IBM Cloud. It takes no input, but
      # puts a quantum element into a state of superposition, where it exists as both
      # a 0 and a 1 simultaneously. It snaps back to a single reality when we take a measurement
      # which the Python code does via the QISKit library.
      # This allows us to basically perform a quantum experiment. We are not "looking at"
      # this measurement -- just bringing it back and setting the state of our "cat" in the box.
      # We will only "know" the output of the quantum computer when we open the box and take
      # a look at the "cat"!
      try:
        # Python 3:
        # nZeroes = urllib.request.urlopen("http://qcat-server.herokuapp.com/run_superposition").read()

        # Python 2:
        print("Calling Heroku gateway...")
        s = "http://qcat-server.herokuapp.com/run_superposition/{}".format(nShots)
        nZeroes = urllib2.urlopen(s).read()
        print("...returned from Heroku gateway.")
        nZeroes = int(nZeroes) # This will throw an exception if we didn't get back a number

        # Reset nShots based on what run_superposition does to reset it if outside of 1 and 1024
        if (nShots < 1 or nShots > 1024):
            nShots = 1

        nOnes   = nShots - nZeroes
      except:
        # Returning nZeroes = 0 (nOnes = 0 also) if there was an error.
        nZeroes = 0
        nOnes   = 1024

      # Since we ran this experiment 1024 times in one shot, via the simulator, we will return
      # the total number of 0s and 1s, in that order.
      return nZeroes, nOnes
