#!/usr/bin/python

import time
#from stepper import PiStepper
from stepper_driver_that_works import PiStepper

CW = 1
CCW  = 0

FROM_ALIVE = CW
FROM_NOT_ALIVE = CCW

# Create a stepper motor controller object
pi_smc = PiStepper()




def go_home(from_position):
    pi_smc.setDirection(from_position)
    pi_smc.step(200)
    time.sleep(0.2)

    pi_smc.nullCoils()
    time.sleep(0.1)

def go_to_alive():
    pi_smc.setDirection(CCW)
    pi_smc.step(200)
    time.sleep(0.2)

    pi_smc.nullCoils()
    time.sleep(0.1)

def go_to_not_alive():
    pi_smc.setDirection(CW)
    pi_smc.step(200)
    time.sleep(0.2)

    pi_smc.nullCoils()
    time.sleep(0.1)

def gage_start():
    pi_smc.start()
    pi_smc.setSpeed(100)
    pi_smc.setPosition(0)

def gage_stop():
    pi_smc.stop()

def main():
    gage_start()

    go_to_alive()
    time.sleep(0.5)

    go_home(FROM_ALIVE)
    time.sleep(0.5)

    go_to_not_alive()
    time.sleep(0.5)

    go_home(FROM_NOT_ALIVE)
    time.sleep(0.5)

    gage_stop()

if __name__=="__main__":
    main()
