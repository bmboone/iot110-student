$(document).ready(function() {

// https://morrisjs.github.io/morris.js/donuts.html
var data = []; // This holds the data for the donut chart
  // It needs to contain label and value keys.

iotSource.onmessage = function(e) {

  console.log("Hi!!");
  console.log(e);
  console.log(e.data);
  d = JSON.parse(e.data);
  console.log(d);

  updateDisplay(d); // Update the display with the new cat info
}

// The main part of it, this updates the display of cats and the donut chart.
updateDisplay = (function (d) {
  console.log("in updateDisplay");
  console.log(d['message']);

  document.getElementById('message').innerHTML = d['message'];

  if (d['new-result'] == 'Y') {
    var obj = {};
    obj['alive'] = d['alive']; // Get this data from new results
    data.push(obj);

    if (data.length > 10) {
      data.shift();
      //clearTable();
      //updateTable(data);
    }

    update_cat_donut(data);



  }
});


// ----------
function clearTable() {
  $('tr.param-row').each(function(i) {
    $(this).empty();
  });
}

function updateTable(data) {
  $('tr.param-row').each(function(i) {
    var tm = '<td>' + data[i]['date'] + '</td>';
    var temp = '<td>' + data[i]['temp'] + '</td>';
    var press = '<td>' + data[i]['press'] + '</td>';
    $(this).append(tm);
    $(this).append(temp);
    $(this).append(press);
  });
}
// -------------

var donut = new Morris.Donut({
  element: 'cat-donut',
  data: [
    {label: "Alive", value: 0},
    {label: "Dead", value: 0}
  ]
});

function update_cat_donut(data) {

  // Count up the # of Alive and Dead cats and set the donut data.
  var alive = 0;
  var dead = 0;

  // Loop through the objs on data and count.
  var arrayLength = data.length;
  var ob;
  var grid_id;

  var cat_alive_pic = "../static/images/cat-fur.png";
  var cat_dead_pic = "../static/images/cat-skel.png";

  var cat_alive_html = "<img src='" + cat_alive_pic + "' alt='Alive' width='129' height='68'>";
  var cat_dead_html = "<img src='" + cat_dead_pic + "' alt='Dead' width='129' height='68'>";

  for (var i = 0; i < arrayLength; i++) {
    ob = data[i];
    if (ob['alive'] == 'Y') {
      alive += 1;
      // Replace the html in this position of the grid with an "alive" cat picture.
      grid_id = 'cat-past-ten-' + i.toString();
      document.getElementById(grid_id).innerHTML = cat_alive_html;
      console.log(cat_alive_html);
    } else {
      dead += 1;
      // Replace the html in this position of the grid with a "dead" cat picture.
      grid_id = 'cat-past-ten-' + i.toString();
      document.getElementById(grid_id).innerHTML = cat_dead_html;
      console.log(cat_dead_html);
    }
  }

  // Now set the donut data...
  var donut_data = [
    { label: "Alive", value: alive},
    { label: "Dead", value: dead}
  ];

  // ...and update the donut itself
  donut.setData(donut_data);
}

});
