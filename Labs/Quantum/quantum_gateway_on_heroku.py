from flask import *
import os
from qiskit import QuantumProgram
import Qconfig
from random import randint

# Barry Boone
# 12/2/17
# This code asks as a (Flask) gateway to accessing the IBM Quantum Computer.
# It is currently set to just use the simulator, but this could be changed
# later to pass in whichever way we want to run -- on the simulator or on the
# real quantum computer.

# This helps access the IBM Quantum Computer by providing url route
# that does a run on the quantum computer (or simulator) and returns the results.
# It's built to do things very straightforwardly as
# I experiment with using the QISKit library and learn how to write quantum programs.

app = Flask(__name__)

@app.route("/")
def index():
    return 'You have reached the Schrodinger Cat server.'

# run_superposition just puts a single qubit into superposition and then measures
# it, returning it to a classical state. It's equivalent to a coin flip in the
# classical workd -- except that it uses a quantum element to the do the coin flip.
@app.route("/run_superposition/<int:nShots>")
def run_superposition(nShots):

  # https://www.qiskit.org/documentation/example_real_backend.html
  Q_program = QuantumProgram()

  # This grabs my token from the Qconfig file that I used to sign up for the
  # IBM cloud services.
  Q_program.set_api(Qconfig.APItoken, Qconfig.config["url"])

  # Create a qubit -- I'm creating 2 of them here for historical reasons,
  # but probably could just create 1 in the current incarnation of this program.
  qr = Q_program.create_quantum_register("qr", 2) # 1

  # Create a regular, classical bit (again creating 2 for historical reasons,
  # but I could simplify this if I wanted to).
  cr = Q_program.create_classical_register("cr", 2) # 1

  # Create a simple circuit with these bits.
  qc = Q_program.create_circuit("superposition", [qr], [cr])

  # Apply the Hadamard gate -- this puts the quantum bit into superposition.
  # Mathematically, we're multiplying a matrix representing the Hadamard gate
  # by the column vector representing the qubit.
  qc.h(qr[0])

  # Measure to reveal the internal state -- this will make the quantum element "choose"
  # which state to be in, which will drop it out of the quantum superposition.
  qc.measure(qr, cr)

  # The program is complete. Now we need to run it...

  # If I pass in 0 (or less, or if it's set to run too many shots) then I just
  # set the number of shots to 1 and just get my own random result. This is useful
  # for testing the logic here without actually hitting the IBM cloud.
  if (nShots < 1 or nShots > 1024):
      nShots = 1
      r1 = randint(0,1)

  # Otherwise, if we didn't pass in 0 (or an illegal value), we hit the cloud.
  else:
     try:
       # Compiled and execute in the simulator: "local_qasm_simulator" or "ibmqx_qasm_simulator"
       # If we were doing this for real, we'd use either:
       # "ibmqx2" --> Online Real Chip, 5 Qbits
       # or
       # "ibmqx3" --> Online Real Chip, 16 Qbits
       result = Q_program.execute(["superposition"], backend='ibmqx_qasm_simulator', shots=nShots)

       # Get the result.
       d = result.get_data("superposition")

       # For results output format, see:
       # https://www.qiskit.org/documentation/_modules/qiskit/simulators/_qasmsimulator.html

       # d looks like this:
       # {'creg_labels': 'cr[2]', 'additionalData': {'seed': 210551591}, 'time': 0.000227196,
       #       'counts': {'00': 5, '01': 5}, 'date': '2017-12-01T01:27:05.492Z'}

       # Note that with only 1 shot, d looks like this:
       # {'creg_labels': 'cr[2]', 'additionalData': {'seed': 1572797684}, 'time': 9.5378e-05,
       #       'counts': {'01': 1}, 'date': '2017-12-01T01:29:36.529Z'}
       # That is, it will have only '01' --OR-- '00' in 'counts', not both. So do the right
       # thing to get the results.

       counts = d['counts']

       # I am just being careful here not to die trying to read the results in case there
       # was some weird error and the IBM Cloud is returning something I can't read.
       try:
          # There will only be a '00' or '01' in a single shot run (with 2 qubits, that is)
          if ('00' in counts):
              r1 = counts['00']

          elif ('01' in counts):
              r1 = nShots - counts['01']

          else:
              r1 = 0 # Returning something but should be some error code probably if we cared

       except:
          # This should never happen; just being safe...
          print("exception in getting counts")
          r1 = 0 # Should be an error code, like -1...

     except:
       # This should never happen, either, as far as I can tell...
       print("exception in call to IBM Q")
       r1 = 0 # Should be an error code, like -1...

  # Return the number of shots that were equal to 0.
  # The caller has to figure out then the number of 1s, based on the number of shots.
  return str(r1)



if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, threaded=True)
