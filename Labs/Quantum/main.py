#!/usr/bin/python

# Barry Boone
# IQoT (Internet of Quantum Things)
# Started 11/29/17

import time
from gpio import PiGpio
from debouncer import Debouncer
from flask import *
from quantum_computer import QuantumComputer

# Going to read over the serial port to get the light sensor
# from the RedBoard (the Arduino basically)
import serial

# Stepper motor
from stepper_driver_that_works import PiStepper

CW = 1
CCW  = 0

FROM_ALIVE = CW
FROM_NOT_ALIVE = CCW

# Create a stepper motor controller object
pi_smc = PiStepper()


current_milli_time = lambda: int(round(time.time() * 1000))

# create GPIO Object and Sensor Debouncer
pi_gpio = PiGpio()
db = Debouncer()
qc = QuantumComputer()

# Misc. values
previousSerialData    = 1      # Start with the box uncovered and detecting light
serialData            = 1

s = "No current quantum results."
RESPONSE_THRESHOLD = 5000   # Hold a response message for 5 seconds

# Helper routines
def lightJustTurnedOn(previousSerialData, serialData):
   return ( (previousSerialData != serialData) and (serialData == 1) )

def lightJustTurnedOff(previousSerialData, serialData):
   return ( (previousSerialData != serialData) and (serialData == 0) )

def clearLastResponse(last_response):
    return (current_milli_time() - last_response > RESPONSE_THRESHOLD)

# Stepper motor stuff:

def gage_go_home(from_position):
    pi_smc.setDirection(from_position)
    pi_smc.step(200)
    time.sleep(0.2)

    pi_smc.nullCoils()
    time.sleep(0.1)

def gage_go_to_alive():
    pi_smc.setDirection(CCW)
    pi_smc.step(200)
    time.sleep(0.2)

    pi_smc.nullCoils()
    time.sleep(0.1)

def gage_go_to_not_alive():
    pi_smc.setDirection(CW)
    pi_smc.step(200)
    time.sleep(0.2)

    pi_smc.nullCoils()
    time.sleep(0.1)

def gage_start():
    pi_smc.start()
    pi_smc.setSpeed(100)
    pi_smc.setPosition(0)

def gage_stop():
    pi_smc.stop()

# ============================== API Routes ===================================
app = Flask(__name__)

@app.route("/")
def index():
    # return "Welcome to the Internet of Quantum Things!"
    return render_template('index.html')


# =========================== Endpoint: /myData ===============================
# read the gpio states by GET method from curl for example
# curl http://10.0.1.37:5000/myData
# -----------------------------------------------------------------------------
@app.route('/startParadox')
def startParadox():
    def keepLooping():

        # Misc. values
        previousSerialData    = 1      # Start with the box uncovered and detecting light
        serialData            = 1

        s = "No current quantum results."
        RESPONSE_THRESHOLD = 5   # Hold a response message for 5 seconds

        # Top left on the usb ports is ttyUSB0
        serialport = serial.Serial("/dev/ttyUSB0", 9600, timeout=3.0)

        last_response = current_milli_time()
        nZeroes = 0
        nOnes = 1024
        checkElapsedTime = False

        gage_status = FROM_ALIVE

        while True:
            # return the yield results on each loop, but never exits while loop

            # We create a Python generator function -- it keeps looping infinitely,
            # and at the yield statement, this function jumps out momentarily to send
            # the response back to the caller.
            # A generator is basically an iterator that keeps returning results to the caller,
            # but then it pops back into the iterator again afterwards

            serialCharData = serialport.readline()
            # print str(serialCharData)

            if (len(serialCharData) > 0):
                serialData = int(serialCharData)
            else:
                serialData = previousSerialData

            # nShots = 0 to just get a random number
            # nShots > 0 and <= 1024, it'll hit the simulator
            nShots = 1

            boxJustCovered = lightJustTurnedOff(previousSerialData, serialData)

            if (boxJustCovered):
                print("Light sensor detects the box was just placed over the breadboard.")

            boxJustUncovered = lightJustTurnedOn(previousSerialData, serialData)

            if (boxJustUncovered):
                print("Light sensor detects the box has just been lifted off the breadboard.")

            previousSerialData = serialData

            if (boxJustCovered):
                print("Calling Q")

                results = {}
                s = "The quantum paradox has been initiated..."
                results['message'] = s
                results['new-result'] = 'N' # No new data yet...
                results['zeroes'] = nZeroes
                results['ones'] = nOnes

                # print("msg: {}".format(s))

                yield('data: {0}\n\n'.format(json.dumps(results)))


                run_qc_on_heroku = True  # switch for testing locally or via heroku gateway I wrote
                if (run_qc_on_heroku):
                    nZeroes, nOnes = qc.runSuperposition(nShots)
                else:
                    nZeroes = 0
                    nOnes = 1   # nOnes >= nZeroes = Alive
                    time.sleep(1.0)

                print("results: zeroes: {} ones: {}".format(nZeroes, nOnes))

                # If the number of ones >= zeroes, then turn the gage to alive
                if (nOnes >= nZeroes):
                    gage_go_to_alive();
                    gage_status = FROM_ALIVE
                else:
                    gage_go_to_not_alive();
                    gage_status = FROM_NOT_ALIVE


                # We're not really going to show this response here once we get
                # this working -- that will only be graphed once the user has
                # opened the box.

                # This has the number of 0s and 1s for debugging purposes.
                # s = 'It is time to lift the box and check on the cat! {} and {}'.format(nZeroes, nOnes)

                # This is JUST the message to check on the cat!
                s = 'It is time to lift the box and check on the cat!'

                results = {}
                results['message'] = s
                results['new-result'] = 'N' # No new data yet...
                results['zeroes'] = nZeroes
                results['ones'] = nOnes

                # print("msg: {}".format(s))

                yield('data: {0}\n\n'.format(json.dumps(results)))



            data_obj = {}

            if (boxJustUncovered):
                last_response = current_milli_time()
                checkElapsedTime = True

                # Now it's okay to report the results to the browser,
                # because the user has lifted the box.
                results = {}
                results['message'] = s
                results['new-result'] = 'Y'
                results['zeroes'] = nZeroes
                results['ones'] = nOnes     # Alive
                results['alive'] = 'Y' if (nOnes >= nZeroes) else 'N'

                yield('data: {0}\n\n'.format(json.dumps(results)))

            # After a while, we'll reset the message to indicate there hasn't
            # been a recent result.
            if (checkElapsedTime):
                if (clearLastResponse(last_response)):
                    s = "No recent quantum results."
                    nZeroes = 0
                    nOnes = nShots

                    checkElapsedTime = False

                    results = {}
                    results['message'] = s
                    results['new-result'] = 'N'
                    results['zeroes'] = nZeroes
                    results['ones'] = nOnes     # Alive
                    results['alive'] = 'Y' if (nOnes >= nZeroes) else 'N'

                    yield('data: {0}\n\n'.format(json.dumps(results)))

                    # Reset the stepper motor
                    gage_go_home(gage_status)


            time.sleep(1.0)
    return Response(keepLooping(), mimetype='text/event-stream')
# ============================== API Routes ===================================

if __name__ == "__main__":
    gage_start()
    app.run(host='0.0.0.0', debug=True, threaded=True)
