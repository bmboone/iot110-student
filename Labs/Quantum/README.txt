README

Barry Boone
Hackathon Project

OVERVIEW

This project is an _actual_ implementation of the paradox of Schrodinger's Cat.
You might consider this part of the Internet of Quantum Things.

Early in the 20th Century, Schrodinger invented a paradox where he connected
a quantum effect to our everyday, classical world. In quantum theory,
a quantum particle only has a definite value when that particle is measured.
Between measurements, quantum theory says that a particle exists in a
"superposition" of multiple states. One mainstream view is that such a
particle exists in multiple "realities" at once.

The way Schrodinger related a quantum effect to a classical result was by
imagining the decay of a particle that would trigger the breaking of a glass
vial containing a poison. Shrodinger imagined placing something "classical"
in a box with this poison vial -- a cat. Since -- so the theory goes -- we
would only know if the particle decayed once we measured the system, then as
long as we didn't look at the box -- that is, as long as we left the system
"unmeasured" -- the particle would exist in both a
decayed and non-decayed state at once. Therefore the cat, also,
would exist in both an alive and not-alive state.

See: https://en.wikipedia.org/wiki/Schr%C3%B6dinger%27s_cat

For this project, I created this exact scenario -- possibly for the
first time!? -- though, fortunately, not with a real cat! I simulated
the two states of a cat on a breadboard via the stepper gage
and a light sensor. When the breadboard is covered with a box, the
Raspberry Pi calls out to the IBM Quantum Computer on the IBM Cloud.
I wrote code using the IBM's Quantum Python
library called QISKit to connect with the IBM Quantum cloud and put a qubit
(i.e., a quantum element) into a state of superposition.

See: https://www.research.ibm.com/ibm-q/

In superposition, this qubit no longer exists in a single state
(representing a 0 or a 1) -- it exists as both a 0 and a 1 at the same time.
(Technically, I applied a Hadamard gate to this qubit, which accomplishes
this superposition.) Then I used the IBM quantum cloud API via the QISKit
library to measure this qubit.

With the measurement, the qubit "snaps back" to a single reality and takes a
definite 0 or 1 value (with equal probability). I communicate this
result to the breadboard circuit, via the Pi, to move the
dial on the gage to the correct state. At this point, the "cat" in the box
is in an undetermined state, both dead and alive, since we have not
measured the state of the system. (The state of the system consists of
the _entire_ system: the gage in the box, yes, but also the computers
involved, such as those on the IBM cloud, as well as the IBM quantum computer.
As a system, we have not _yet_ performed our "measurement," according
to the Copenhagen Interpretation of quantum mechanics, because we have
not (consciously) looked at it; if we do not look, the system is still
unmeasured. Lifting the box reveals the result --
what state the qubit was/is in, and so what state the cat is in! Only on
lifting the box does the web interface
to the Raspberry Pi indicate what the result of the cat is.

Whether you literally believe that the cat is both dead and alive in its
unmeasured state depends on how literally you believe the Copenhagen
Interpretation of quantum mechanics, which as worked out in the 1920s.

See: https://en.wikipedia.org/wiki/Copenhagen_interpretation

The web interface also keeps track of the previous 10 cat outcomes via
a donut chart using the Morris javascript library.

Just a note about the IBM quantum computer: while the QISKit program I
wrote _could_ run on the _actual_ quantum computer in the IBM cloud, for
this class, I set the program to run using IBM's quantum simulator.
Since I am allowed only a small number of runs on IBM's
real quantum computer, I don't want to use up my allocation too quickly!
However, the IBM quantum computer simulator should work very well to
create an IoT Schrodinger's Cat.

ARCHITECTURE and FLOW

Light sensor -> Arduino (running on the Arduino because of the analog
nature of the light sensor) -> serial communication -> Pi

Pi running Flask/Python code, periodically reads USB/serial port usb0. When
the light sensor indicates the box is closed, the Pi calls out to code I
wrote using IBM's QISkit quantum library. (I had trouble getting this
library to run on the Pi, due to it wanting to run via Anaconda 3.6. So
I set up a server and app using Heroku,
and have the QISkit code running there to support this project, acting as a
gateway for the Pi to reach the IBM Quantum Cloud.)

So:

Close lid of box.

Light sensor let's Pi know that (via the Arduino serial connection)

Pi tells the browser connected via the Flask app:
        indicate the paradox has been initiated

Pi hits the url for the Heroku app, built using Python, Flask,
    and QISkit, which then calls the IBM Quantum cloud & quantum computer

IBM Quantum Cloud returns a result back to Heroku, which returns the
    result back to the Pi

Pi then move gage dial to the correct cat picture

Pi also talks to the browser connected to the Flask app:
    indicate new results are ready

And then...

light sensor detects when the box is opened, the Arduino tells the Pi this
    via the serial port to the Pi

Pi tells the browser connected to the Flask app: update the results in
the browser with the new data (It's okay to show these results now --
the user is looking in the box.)

Pi starts to time out the results... After a few seconds, the Pi reset the gage,
and also indicates on the connected browser page, via the Flask, that 
there are no recent results
