[LAB8 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab9/setup.md)

## Part B - Create an instance of Azure IoT Hub

**Synopsis:** In this part, we will configure Azure IoT Hub.

## Objectives

* Explore IoT Hub
* Provision an Azure IoT Hub

### Step B1: Create Azure IoT Hub

Review [Azure IoT Hub](https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-what-is-iot-hub) introduction.

From Azure Portal choose the IoT Hub under "Internet of Things", and follow provisioning instructions:

![Azure IoT Hub](https://gitlab.com/iot110/iot110-student/raw/master/Resources/Images/Lab9-CreateIoTHub.png)

### Step B2: Setup iothub-explorer on RPi

Install iothub-explorer, a cli for the IoT Hub we recently created, and the azure-iothub-device-client library,
which is used in the app.py example Python script.

```sh
pi$ sudo apt-get update
pi$ sudo apt-get install libboost-python1.55-dev
pi$ sudo pip install azure-iothub-device-client
pi$ sudo apt-get install nodejs npm
pi$ sudo npm install -g iothub-explorer
```

### Step B3: Login to IoT Hub and create device via CLI

First login to the iothub-explorer.  This will create a session that will be valid for a short time and is saved between cli calls.

```sh
pi$ iothub-explorer login "HostName=<<host>>;SharedAccessKeyName=service;SharedAccessKey=<<key>>"
```

Then create a new device, either from the cli:

```sh
pi$ iothub-explorer create <device name>
```

Or from the IoT Hub control panel:

1. From the Azure Portal, select the IoT Hub
1. In the IoT Hub control panel, under Explorers select IoT Devices
1. Select "Add"
1. Add an appropriate name and click "Save" (all other options are acceptable: symmetric keys, auto generated, enabled interaction with IoT Hub)
1. Select the newly created device
1. Make a note of the "Connection String--Primary Key", which will be used to connect the RPi


### Step B4: List devices on IoT Hub via iothub-explorer

```sh
pi$ iothub-explorer list

pi$ iothub-explorer list |  grep <device name>
pi$ iothub-explorer list |  grep bar
```

### Step B5: Device to Cloud (D2C) Communication via IoT Hub

Log into the iothub-explorer using the service level to watch for events that are sent from devices to the IoT Hub:

```sh
pi$ iothub-explorer monitor-events --login "HostName=<<hostname>>;SharedAccessKeyName=service;SharedAccessKey=<<psk>>"
```

Then send a sample event from the cli:

```sh
pi$ iothub-explorer login "<<device connection string>>"
pi$ iothub-explorer simulate-device <device name> --send "{deviceId: '<<device>>', windSpeed: 10.671534826979041 }"
```
### Step B6: Cloud to Device Communication via IoT Hub

```sh
pi$ iothub-explorer simulate-device <device name> --receive
```

Then in another terminal enter the following to send a message from the cloud to the [simulated] device:

```sh
pi$ iothub-explorer send <device name> "Test Message!"
```

### Step B7: Using IoT Hub from Python

Lets do a quick review of the Python code in app.py, then run o demontrate:

```sh
pi$ #Start the iothub-explorer event monitor to see the received data:
pi$ iothub-explorer monitor-events --login "HostName=<<hostname>>;SharedAccessKeyName=service;SharedAccessKey=<<psk>>"
pi$ #Start the python code to send data
pi$ python app.py "<<device connection string>>"
pi$ #Send the device a C2D message
pi$ iothub-explorer send <device name> "Test Message!"
```

[PART A](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab9/PartA.md)

[LAB8 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab9/setup.md)
