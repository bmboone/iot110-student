README

Barry Boone
Lab 5
11/14/17

Made sure everything was running right for Lab 5, including making the pressure chart
run similar to the temperature chart.

Used some of the work done for Labs 3 and 4, and also used the basic code from
class and the notes for Lab 5 to create tables and a tab interface to show
the temperature and pressure data from the chip via the Pi.

Included three screenshots.
