SHIFT_MASK = 15 # 15 for 4 bits of 1's
DEBOUNCE_INTERVAL = 200 # number of milliseconds

import time

current_milli_time = lambda: int(round(time.time() * 1000))

class Debouncer(object):
  """Shift Register Debouncer"""

  def __init__(self):
     self.switch_shift_register = 0
     self.interval = DEBOUNCE_INTERVAL
     self.current_switch = 0
     self.previous_change_millis = 0

  # performa AND logic debouncer
  # This goes to 1 only if the previous 4 reads were 1, but it goes to
  # 0 immediately on reading a 0.
  def debounce(self, switch_in):
     self.switch_shift_register = (self.switch_shift_register << 1) | switch_in
     return int((self.switch_shift_register & SHIFT_MASK) == SHIFT_MASK)

  # This only switches the state if it reads the same (new) switch value
  # after a certain interval of it being the same.
  def debounce_using_time_interval(self, switch_in):

      # Switch has changed
      if (switch_in != self.current_switch):
          if (self.previous_change_millis == 0):

              # This is the first time we've noticed the switch has changed,
              # so for now, just save the current time in millis.
              # When enough time
              # has gone by to verify this is now the new, stable switch
              # value, then change the internal state to the new switch value.
              self.previous_change_millis = current_milli_time()
          else:
              if ( (current_milli_time() - self.previous_change_millis) > self.interval):
                  # Okay, the switch has changed, and we're past an interval for
                  # when we last read the same value
                  # for the switch, it's changed, clearly, so reset it all.
                  self.current_switch = switch_in
                  self.previous_change_millis = 0 # reset so we can start all over again next time
      return self.current_switch
