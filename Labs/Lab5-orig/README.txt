README File

Barry Boone
October 31, 2017
Lab 3

I created a custom graphical interface for this lab that uses some nice
slider/switch UI elements, images I created in Photoshop of the LEDs both off and
glowing, and a schematic-looking switch to represent the switch on the board.

When the client (browser) connects with the server, the html initializes itself
to the current state of the LEDs.

I also tried a variation of the debouncer code that uses a time-based debouncing
algorithm to make sure a small interval of time has passed with the new switch
state before changing over to the new switch state.

In addition to the gpio code, flask server and html
template for setting and getting the gpio pins and
board components via the Pi, I also uploaded two screenshots and
a short video of the UI, code, and setup in action.
