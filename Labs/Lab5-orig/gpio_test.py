#!/usr/bin/python

# Barry Boone
# October 22, 2017
# This code is based on the code samples from the IoT class in Lab2
# This is just test code that sequences through the lights on the board
# to see if the lights blink. If they do, all is well with the world.

import time
from gpio import PiGpio
    # We just defined this very nice class, PiGpio, in the file (ie the module) gpio (gpio.py).
    # Let's import that class and use it here.

# create an instance of the pi gpio driver.
pi_gpio= PiGpio()

# I'm going to set this as a variable in case I change it later.
time_between_blinks = 1.0

# Blink the LEDS forever.
print('Blinking all my LEDs in sequence (Ctrl-C to stop)...')
while True:
    # Get the current switch state and print
    # (We don't sequence through this -- the user has to press the button and
    # then when this code cycles around to here again to check it, the info about
    # the switch prints to the console.
    switch = pi_gpio.read_switch() # Using what we defined in the PiGpio class in gpio.py
    print('\n============ Switch: {0} ============'.format(switch))

    # Turn everything ON
    print('\nLED 1 ON (RED)')
    pi_gpio.set_led(1,True) # Using what we defined in the PiGpio class in gpio.py
    print('LED1: {0}'.format(pi_gpio.get_led(1)))   # It's going to display a 0 or a 1 in place of {0}
                                                    # depending on the returned value from pi_gpio.get_led(1)
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(time_between_blinks)

    print('\nLED 2 ON (GRN)')
    pi_gpio.set_led(2,True)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(time_between_blinks)

    print('\nLED 3 ON (BLU)')
    pi_gpio.set_led(3,True)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(time_between_blinks)


    # Turn everything off
    print('\nLED 1 OFF (RED)')
    pi_gpio.set_led(1,False)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(time_between_blinks)

    print('\nLED 2 OFF (GRN)')
    pi_gpio.set_led(2,False)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(time_between_blinks)

    print('\nLED 3 OFF (BLU)')
    pi_gpio.set_led(3,False)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(time_between_blinks)
