# Barry Boone
# October 22, 2017
# This code is based on the code samples from the IoT class in Lab2

# Here's the documentation for RPi.GPIO:
# https://sourceforge.net/p/raspberry-gpio-python/wiki/Home/

import RPi.GPIO as GPIO

LED1_PIN    = 18    # cobbler pin 12 (GPIO18)
LED2_PIN    = 13    # cobbler pin 33 (GPIO13)
LED3_PIN    = 23    # cobbler pin 16 (GPIO23)
SWITCH_PIN  = 27    # cobbler pin 7  (GPIO27)

# As per this question: https://stackoverflow.com/questions/4015417/python-class-inherits-object
# and this answer: https://docs.python.org/release/2.2.3/whatsnew/sect-rellinks.html
# we are defining a "new style" object (well, class, really) for PiGpio --
# which gives it attributes and other nice new Python things.
class PiGpio(object):
    """Raspberry Pi Internet 'IoT GPIO'."""
    
    # Okay, as per the documentation at
    # https://sourceforge.net/p/raspberry-gpio-python/wiki/BasicUsage/
    # we're setting up channels -- pins set up as GPIO.IN or GPIO.OUT.

    def __init__(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM) # So we're using the lower-level numbering here, the other option is GPIO.BOARD
        GPIO.setup(LED1_PIN, GPIO.OUT)      # RED LED as output
        GPIO.setup(LED2_PIN, GPIO.OUT)      # GRN LED as output
        GPIO.setup(LED3_PIN, GPIO.OUT)      # BLU LED as output
        GPIO.setup(SWITCH_PIN, GPIO.IN)     # Switch as input w/pu
        GPIO.setup(SWITCH_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def read_switch(self):
        """Read the switch state."""
        switch = GPIO.input(SWITCH_PIN)
        # invert because of active low momentary switch
        # (That is -- we set the pin for the switch as a pull-up pin, meaning
        # that it is "on" when we do not explicitly send current to it, and it
        # is "off" when we do. Since we are returning the state of the switch, not
        # the state of the pin, we have to invert what we read from the pin.)
        if (switch == 0):
            switch=1
        else:
            switch=0
        return switch

    # set the particular LED to ON or OFF
    # Simple enough -- GPIO.output makes this trivial.
    def set_led(self, led, value):
        """Change the LED to the passed in value, '1' ON or '0' OFF."""
        if(led == 1):
            GPIO.output(LED1_PIN, value) # Sets the state.
        if(led == 2):
            GPIO.output(LED2_PIN, value)
        if(led == 3):
            GPIO.output(LED3_PIN, value)

    # get the state of an LED
    # GPIO.input makes this simple.
    def get_led(self, led):
        """Return the state value of the LED, '1' ON or '0' OFF."""
        if(led == 1):
            return GPIO.input(LED1_PIN)
        if(led == 2):
            return GPIO.input(LED2_PIN)
        if(led == 3):
            return GPIO.input(LED3_PIN)
