#!/usr/bin/python
# OVERVIEW for Lab4:
# The BMP280 chip is the barometric pressure and temperature chip,
# and what we're doing in this lab is setting it up to communicate
# with the Pi via I2C. We're doing this by enabling I2C on the Pi,
# then using a Python implementation of a C library for
# Broadcom BCM 2835 to read the chip's data.
# We need to get that Python package, python-smbus,
# and of course we have to wire up the chip to the Pi after
# soldering on the pins to the chip.
#
# =============================================================================
#        File : bmp280.py
# Description : Read data from the Bosch digital pressure sensor.
#    Authored : Barry Boone (tweaked and added my own notes)
#      Credits: Adapted from S. Dame, who adapted it from Matt Hawkins' code)
#        Date : 11/2/17
# =============================================================================
#
#  Official datasheet available from :
#  https://www.bosch-sensortec.com/bst/products/all_products/bme280
#
# Matt Hawkin's site --> http://www.raspberrypi-spy.co.uk/
# =============================================================================
#
# My notes, 11/4:
# First enable I2C on the Pi
# Also install python-smbus and i2c-tools like this:
#
# sudo apt-get install -y python-smbus i2c-tools
#
# The -y option gives automatic 'yes' to all prompts
# After rebooting you can see the i2c, you can lsmod to see the i2c modules
# The command 'lsmod' lists the modules that can be "hot-swapped" into the kernal
# By piping it to grep, we can see the i2c components, like this:
#
# lsmod | grep i2c_
#
# This will show at least i2c_bcm2835 (which is the version of bcm
# from Adafruit).
# Can also see that we have one device on the I2C bus by doing this:
#
# ls -l /dev/i2c*
#
# That command seems super-useful! If it's working, it shows:
#
# crw-rw---- 1 root i2c 89, 1 Nov  2 19:59 /dev/i2c-1
#
# The "c" in the first character of the results shows that the file is a
# "special character file" --
# meaning that it is a device file that provides serial access, in this case via I2C.
#
# We're going to use the following pins in I2C:
# SCK: serial clock (SCL)
# SDI: data (SDA)
# SDO: Slave address LSB (GND = '0', VDDIO = '1')
#
# Run:
#
# sudo i2cdetect -y 1
#
# and it will scan the bus for devices. It outputs a table with the
# specified devices on the specified bus (according to the man page for this).
# The -y just means it runs the command directly without asking the user (you)
# whether it is okay to run the command. The 1 means we're scanning bus 1.
# If you do not use -y, i2cdetect tells you it will "prove address range 0x03 - 0x77"
# and asks for confirmation that this is okay.
#
# Once all of the above works, then we're good to have this Python program
# in this file work to read the low-level communication of bits and bytes,
# and format the results and send them back to some caller.
# All the caller will have to do is set up an object of the class defined
# here and request the data. So this code hides a lot of low-level stuff
# from consumers of this class.


import smbus    # System Management Bus
import time     # time utilities

# =============================================================================
# ctypes is an advanced ffi (Foreign Function Interface) package for Python
# that provides  call functions in dlls/shared libraries and has extensive
# facilities to create, access and manipulate simple and complicated
# C data types in Python
# =============================================================================
from ctypes import c_short
from ctypes import c_byte
from ctypes import c_ubyte

def getShort(data, index):
  # return two bytes from data as a signed 16-bit value
  return c_short((data[index+1] << 8) + data[index]).value

def getUShort(data, index):
  # return two bytes from data as an unsigned 16-bit value
  return (data[index+1] << 8) + data[index]

def getChar(data,index):
  # return one byte from data as a signed char
  result = data[index]
  if result > 127:
    result -= 256
  return result

def getUChar(data,index):
  # return one byte from data as an unsigned char
  result =  data[index] & 0xFF
  return result

# =============================================================================
# create a sensor object
# -----------------------------------------------------------------------------

DEVICE = 0x76 # We saw that we're on address 0x76 when we ran i2cdetect on the Pi

class PiBMP280(object):
    """Raspberry Pi 'IoT BMP280 Sensor'."""

    # addr = I2C Chip Address
    def __init__(self,addr=DEVICE):

        # We saw we're on bus 1 for the chip we're interested in, also via i2cdetect
        self.bus = smbus.SMBus(1)   # Rev 2 Pi, Pi 2 & Pi 3 uses bus 1
                                    # Rev 1 Pi uses bus 0
        self.addr = addr            # register the address with the object

    def readBMP280ID(self):
      REG_ID     = 0xD0  # This is from the specs and is the address of the chip id
      (chip_id, chip_version) = self.bus.read_i2c_block_data(self.addr, REG_ID, 2)
      return (chip_id, chip_version)

    def readBMP280All(self, addr=DEVICE):
      # Register Addresses
      REG_DATA = 0xF7
      REG_CONTROL = 0xF4
      REG_CONFIG  = 0xF5

      # Oversample setting - page 27
      OVERSAMPLE_TEMP = 2
      OVERSAMPLE_PRES = 2
      MODE = 1

      # write oversample configuration and mode
      control = OVERSAMPLE_TEMP<<5 | OVERSAMPLE_PRES<<2 | MODE
      self.bus.write_byte_data(self.addr, REG_CONTROL, control)

      # LOTS of low-level reading of data here, I didn't write any of the code
      # that follows -- just used the example code provided in class (BB)
      #
      # Read blocks of calibration data from EEPROM
      # See Page 22 data sheet
      cal1 = self.bus.read_i2c_block_data(self.addr, 0x88, 24)
      cal2 = self.bus.read_i2c_block_data(self.addr, 0xA1, 1)
      cal3 = self.bus.read_i2c_block_data(self.addr, 0xE1, 7)

      # Convert byte data to word values
      dig_T1 = getUShort(cal1, 0)
      dig_T2 = getShort(cal1, 2)
      dig_T3 = getShort(cal1, 4)

      dig_P1 = getUShort(cal1, 6)
      dig_P2 = getShort(cal1, 8)
      dig_P3 = getShort(cal1, 10)
      dig_P4 = getShort(cal1, 12)
      dig_P5 = getShort(cal1, 14)
      dig_P6 = getShort(cal1, 16)
      dig_P7 = getShort(cal1, 18)
      dig_P8 = getShort(cal1, 20)
      dig_P9 = getShort(cal1, 22)

    # Don't know what's up with these commented out lines (BB)
    #   dig_H1 = getUChar(cal2, 0)
    #   dig_H2 = getShort(cal3, 0)
    #   dig_H3 = getUChar(cal3, 2)
      #
    #   dig_H4 = getChar(cal3, 3)
    #   dig_H4 = (dig_H4 << 24) >> 20
    #   dig_H4 = dig_H4 | (getChar(cal3, 4) & 0x0F)
      #
    #   dig_H5 = getChar(cal3, 5)
    #   dig_H5 = (dig_H5 << 24) >> 20
    #   dig_H5 = dig_H5 | (getUChar(cal3, 4) >> 4 & 0x0F)

    #   dig_H6 = getChar(cal3, 6)

      data = self.bus.read_i2c_block_data(self.addr, REG_DATA, 6)
      pres_raw = (data[0] << 12) | (data[1] << 4) | (data[2] >> 4)
      temp_raw = (data[3] << 12) | (data[4] << 4) | (data[5] >> 4)

      # Refine temperature based on calibration per spec
      var1 = ((((temp_raw>>3)-(dig_T1<<1)))*(dig_T2)) >> 11
      var2 = (((((temp_raw>>4) - (dig_T1)) * ((temp_raw>>4) - (dig_T1))) >> 12) * (dig_T3)) >> 14
      t_fine = var1+var2
      temperature = float(((t_fine * 5) + 128) >> 8);

      # Refine pressure and adjust for temperature
      var1 = t_fine / 2.0 - 64000.0
      var2 = var1 * var1 * dig_P6 / 32768.0
      var2 = var2 + var1 * dig_P5 * 2.0
      var2 = var2 / 4.0 + dig_P4 * 65536.0
      var1 = (dig_P3 * var1 * var1 / 524288.0 + dig_P2 * var1) / 524288.0
      var1 = (1.0 + var1 / 32768.0) * dig_P1
      if var1 == 0:
        pressure=0
      else:
        pressure = 1048576.0 - pres_raw
        pressure = ((pressure - var2 / 4096.0) * 6250.0) / var1
        var1 = dig_P9 * pressure * pressure / 2147483648.0
        var2 = pressure * dig_P8 / 32768.0
        pressure = pressure + (var1 + var2 + dig_P7) / 16.0

      return temperature/100.0,pressure/100.0

# =============================================================================
# main to test from CLI
# So could just run this from the command line and it'll execute
# the following code to test reading the chip data over the I2C bus
# via all the slinging of bits and bytes in the code above.
# Doing that returns the Chip ID (88), Version (1), Temperature (20.77 C),
# and Pressure (1003.268-etc- hPa)
def main():

    # create an instance of my pi bmp280 sensor object
    pi_bmp280 = PiBMP280()

    # Read the Sensor ID.
    (chip_id, chip_version) = pi_bmp280.readBMP280ID()
    print "    Chip ID :", chip_id
    print "    Version :", chip_version

    # Read the Sensor Temp/Pressure values.
    (temperature, pressure) = pi_bmp280.readBMP280All()
    print "Temperature :", temperature, "C"
    print "   Pressure :", pressure, "hPa"

if __name__=="__main__":
   main()
