README

Barry Boone
Lab 6
11/16/17

Completed in class -- got the SenseHat working and enabled the InertialTable
and InertialChart tabs in the UI.

Uploaded 4 screenshots of this working, including showing the cool line chart
of the inertial chart when you move the SenseHat around to change the pitch,
roll, and yaw. 
