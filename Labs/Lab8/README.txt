Barry Boone
Lab 8
12/14/17

The MQTT protocol is fantastic! I love these lightweight things to construct applications
with -- Flask, MQTT, Raspberry Pi and Arduino even -- they all feel like the modern way to
make systems work. I like that with these simple, easy to understand pieces we can end up
with something that feels magical.
Nothing is too fancy or complicated, everything is lightweight and does one
job and does it well -- no one pieces tries to solve every problem there is -- each piece
solves one part of the problem. Tie it all together and it becomes interesting. The whole
really is greater than the sum of its parts!

Here, I've mostly used the code on the Gitlab repository for the class, tweaking it as
necessary to get it all to work with my Pi and Mac.

I included a short movie of one publisher and two subscribers listening in.

As a side note, I had to follow a different installation than what was in the class notes
to get it all to install on my Pi, but it wasn't too difficult. This is what worked for me:

http://www.instructables.com/id/Installing-MQTT-BrokerMosquitto-on-Raspberry-Pi/

(Just a note if I go back to this in the future: there is an obvious typo/error
in the instructions about the middle of Step 1 where it says, for jessie, to use:

sudo wget http://repo.mosquitto.org/debian/mosquitto-wheezy.list

but should be:

sudo wget http://repo.mosquitto.org/debian/mosquitto-jessie.list

Otherwise it all installed and worked flawlessly.

(Also just for future reference: the sense hat seemed to not _always_ work perfectly --
it really had to be pressed down tight on the Pi, or I'd get errors running it. Sometimes
I physically had to hold the hat clamped down over the GPIO pins to get the mqtt_client
code to work and access the sense hat data.)
