#!/usr/bin/python

# This _could_ really act as both a subscriber and publisher. Right now, it just
# acts as a publisher. It connects to the MQTT broker, then gets sense hat data
# (just as a demo, for fun, really, though this data could be anywhere and from
# any (real) source) and publishes it via the MQTT broker via the publish() method.
import time
import socket
import json

# Needed to install this as per:
# http://www.instructables.com/id/Installing-MQTT-BrokerMosquitto-on-Raspberry-Pi/
import paho.mqtt.client as mqtt
from sense import PiSenseHat

# Not reading my host name, but this works for now...
myHostname = '10.0.1.29'

localBroker = "localhost"   # Local MQTT broker
localPort   = 1883          # Local MQTT port
localUser   = "pi"          # Local MQTT user
localPass = "raspberrycherry"     # Local MQTT password (!) Should be an env var probably
localTopic = "iot/sensor"   # Local MQTT topic to monitor -- this should probably be an env var too
localTimeOut = 120          # Local MQTT session timeout

# The callback for when a CONNACK message is received from the broker.
def on_connect(client, userdata, flags, rc):
    print("CONNACK received with code %d." % (rc))

# The callback for when a PUBLISH message is received from the broker.
def on_message(client, userdata, msg):
    # Just simply displaying the info but could obviously do something with it!
    print (string.split(msg.payload))

# display one row of blue LEDs (just for fun)
def displayLine(row):
    blue = (0, 0, 255)
    for i in range(0,8):
        x = i % 8
        y = (i / 8) + row
        # print("set pixel x:%d y:%d" % (x,y))
        pi_sense.set_pixel(x,y,blue)
        time.sleep(0.02)

# Create a sense-hat object
pi_sense = PiSenseHat()
print('PI SenseHat Object Created')

# Setup to Publish Sensor Data
# This is super-easy to use: just create the client, say what to do on connect and
# on receiving a message, log in, and voila!
# Publish via publish() below

mqttc = mqtt.Client()
mqttc.on_connect = on_connect
mqttc.on_message = on_message
mqttc.username_pw_set(localUser, localPass)
mqttc.connect(localBroker, localPort, localTimeOut)

# initialize message dictionary
msg = {'topic':localTopic, 'payload':"", 'qos':0, 'retain':False }

#pi_sense.clear_display()
# loop
print('Getting Sensor Data')
for i in range(1,9):
    print("SensorSet[%d]" % (i))
    #displayLine(i-1)
    sensors = pi_sense.getAllSensors()

    sensors['host'] = myHostname
    msg['payload'] = json.dumps(sensors)
    print("msg["+str(i)+"]:"+msg['payload'])

#   publish(topic, payload=None, qos=0, retain=False)
    mqttc.publish(msg['topic'], msg['payload'], msg['qos'])
    time.sleep(2.0)

print('End of MQTT Messages')
quit()
