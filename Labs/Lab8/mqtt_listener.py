#!/usr/bin/python

# This is just a simple listener, without any publish() implementation,
# just to be able to easily set up multiple listners. It connects and
# subscribes to the MQTT broker, then deals with a message it receives via
# implementing on_message.
import time
import paho.mqtt.client as paho
import json
import ast

# See notes for client. Same here.
localBroker = "localhost"     # Local MQTT broker
localPort   = 1883          # Local MQTT port
localUser   = "pi"          # Local MQTT user
localPass = "raspberrycherry"     # Local MQTT password
localTopic = "iot/sensor"   # Local MQTT topic to monitor
localTimeOut = 120          # Local MQTT session timeout

# The callback for when a CONNACK message is received from the broker.
# Subscribe to a topic here. (Could subscribe elsewhere too...)
def on_connect(client, userdata, flags, rc):
    print("CONNACK received with code %d." % (rc))
    mqttc.subscribe(localTopic, 0)

# What to do when receiving a message.
def on_message(mosq, obj, msg):
    global message
    print("Topic Rx:" + msg.topic + " QoS:" + str(msg.qos) + "\n")
    clean_json = ast.literal_eval(msg.payload)
    print ("Payload: ")
    # import pdb; pdb.set_trace()
    print json.dumps(clean_json , indent=4, sort_keys=True)
    # message = msg.payload
    # This is where we will develop client connect to  Azure IoT Suite
    # mqttc.publish(cloudTopic,msg.payload, 1);

print('Establish MQTT Broker Connection')
# Setup to listen on topic`
mqttc = paho.Client()
mqttc.on_connect = on_connect
mqttc.on_message = on_message
mqttc.username_pw_set(localUser, localPass)
mqttc.connect(localBroker, localPort, localTimeOut)
print('MQTT Listener Loop <ctl-C> to break...')
mqttc.loop_forever()
