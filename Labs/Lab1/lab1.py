# Barry Boone
# October 13, 2017
# 
# First assignment for IoT 110, which was to get 
# our RPi up and running, get a gitlab account, and
# be able to get a Flask page server going on the RPi.
#
# 1. Flashed an SD card with Raspbian
# 2. Booted up the Pi (set local preferences, turned on SSH, changed
#    the default password, etc.)
# 3. Set up a gitlab account under bmboone, including putting in my public 
#    ssh rsa key. Set up a new (blank) project called iot110-student under
#    my new account.
# 4. Got the original code for the class like this:
#    git clone https://gitlab.com/iot110/iot110-student.git
# 5. Cloned from my new (bmboone) iot110-student epository onto my Mac, set up the 
#    Lab/Lab1 directory structure, and copied in the code
#    retrieved from the iot110 repository for Lab1 into a new Python file.
# 6. Experimented, made changes to the code, set up other pages to serve.
# 7. Tested it on the Mac, then used scp to put it onto the Pi and run and test the Flask 
#    server code (this file) there.
# 8. On the Mac, went to the webpages defined in this server code via the Pi's
#    local IP address and the 5000 port, like this: http://10.0.1.37:5000
# 9. Added the code via git to the local repository and pushed it up to gitlab.


from flask import Flask
import socket
from datetime import datetime

# A global variable to test counting how many pages were served
# since the last time this Flask server was restarted
pagecount = 0

## Get my machine hostname
if socket.gethostname().find('.') >= 0:
    hostname=socket.gethostname()
else:
    hostname=socket.gethostbyaddr(socket.gethostname())[0]

app = Flask(__name__)

# A default route -- just returns a hello message.
@app.route("/")
def hello():
  global pagecount
  pagecount += 1
  return "Hello IoT World from RPi3: " + hostname
    
# Experiment to return the current time.
@app.route("/time")
def time():
  global pagecount
  pagecount += 1
  return "The current time is: " + datetime.now().strftime('%Y-%m-%d %H:%M:%S')

# Experiment to show how many pages were served since last server restart.
@app.route("/count")
def count():
  global pagecount
  pagecount += 1
  return "Page views since last server restart: " + str(pagecount)        

# Run the website and make sure to make
# it externally visible with 0.0.0.0:5000 (default port)
# (My RPi has a local IP address of 10.0.1.37)
if __name__ == "__main__":
    app.run(host='0.0.0.0')