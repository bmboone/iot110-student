#!/usr/bin/python
# BB: Used this code from the class asssignment to test the class handling
# low-level communication for the BMP280 chip created in bmp280.py
# (However, bmp280 also has a main() function which is a nice idea, so that it
# can be invoked directly from the command line to test it, so we can also
# test it like that.)
import pprint
import time
from bmp280 import PiBMP280

# constants
NUM_READINGS = 10
accumulated_temps = 0.0
accumulated_pressures = 0.0

# create an array of my pi bmp280 sensor dictionaries
sensor = []

# Note that 0x76 was acquired from running i2cdetect
sensor.append({'name' : 'bmp280', 'addr' : 0x76, 'chip' : PiBMP280(0x76) , 'data' : {}})

# Read the Sensor ID for 0x76 -> values into the ['data'] dictionary
(chip_id, chip_version) = sensor[0]['chip'].readBMP280ID()
sensor[0]['data']['chip_id'] = chip_id
sensor[0]['data']['chip_version'] = chip_version

print "  ============================== SENSOR   =============================="
print "  Chip ADDR :", hex(sensor[0]['addr'])
print "    Chip ID :", sensor[0]['data']['chip_id']
print "    Version :", sensor[0]['data']['chip_version']

# Read the Sensor Temp/Pressure values into the ['data'] dictionary

# Just for fun I'm creating code that takes the average of 10 readings, each
# one made once per second.

for i in range(NUM_READINGS):

   print "  ========== CURRENT READING: ", i+1, " of ", NUM_READINGS, "   ============="

   (temperature, pressure) = sensor[0]['chip'].readBMP280All()
   sensor[0]['data']['temperature'] = { 'reading': temperature, 'units' : 'C' }
   sensor[0]['data']['pressure'] = { 'reading': pressure, 'units' : 'hPa' }

   print "Temperature :", sensor[0]['data']['temperature']['reading'], "C"
   print "   Pressure :", sensor[0]['data']['pressure']['reading'] , "hPa"

   pprint.pprint(sensor[0])

   accumulated_temps += sensor[0]['data']['temperature']['reading']
   accumulated_pressures += sensor[0]['data']['pressure']['reading']

   time.sleep(1) # wait for 1 second

print "  ======================================================================"

print "Average temperature after ", NUM_READINGS, " readings: ", accumulated_temps/NUM_READINGS, " C"
print "Average presure after ", NUM_READINGS, " readings: ", accumulated_pressures/NUM_READINGS, " hPa"

print "  ======================================================================"
