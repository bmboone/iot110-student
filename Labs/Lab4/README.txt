Barry Boone
Lab 4
11/4/17

Soldered the pins onto the BMP280, then basically followed the example code
and directions for the lab to enable I2C (I-squared-C), wired the chip
onto the board, then made sure that device was found on the I2C bus by
running i2cdetect. Got 0x76 so used that address in the code.

Ran bmp280.py to make sure it could communicate with the chip and
get the temperature and pressure data.

I did update bmp280_test.py just for fun to get the average temp and pressure
over some number of readings made once per second (I defaulted to 10 readings).

Also uploaded screenshots of the command line interface results.
