// start executing only after document has loaded
$(document).ready(function() {
  // establish global variables for LED status
  var led1;
  var led2;
  var led3;

  // An attempt to do the listener
  // var iotSource = new EventSource("{{ url_for('myData') }}");

  // The documentation in the lab code says this will
  // "intercept the incoming states from SSE"
  var iotSource = new EventSource("http://10.0.1.37:5000/myData");

  // I could also do this by eventSource.addEventListener = (<any_message>, function(e) {})
  iotSource.onmessage = function(e) {
    console.log(e.data);
    var params = e.data.split(' ');
    updateSwitch(params[0]);
    updateLeds(1,params[1]);
    updateLeds(2,params[2]);
    updateLeds(3,params[3]);
  }

  // update the Switch based on its SSE state monitor
  function updateSwitch(switchValue) {
    if (switchValue === '1') {
      hide_image('switch-opened');
      show_image('switch-closed');
    } else if (switchValue === '0') {
      hide_image('switch-closed');
      show_image('switch-opened');
    }
  }

  // update the LEDs based on their SSE state monitor
  function updateLeds(ledNum,ledValue) {

    // Red LED
    if (ledNum === 1) {
      if (ledValue === '1') {
        hide_image('red-off');
        show_image('red-on');

        // Set the checkbox to match.
        $('#red-checkbox').prop('checked', true);

        led1 = "ON"
      } else if (ledValue === '0') {
        hide_image('red-on');
        show_image('red-off');

        // Set the checkbox to match.
        $('#red-checkbox').prop('checked', false);

        led1 = "OFF"
      }
    }

    // Green LED
    else if (ledNum === 2) {
      if (ledValue === '1') {
        hide_image('green-off');
        show_image('green-on');

        // Set the checkbox to match.
        $('#green-checkbox').prop('checked', true);

        led2 = "ON"
      } else if (ledValue === '0') {
        hide_image('green-on');
        show_image('green-off');

        // Set the checkbox to match.
        $('#green-checkbox').prop('checked', false);

        led2 = "OFF"
      }
    }

    // Blue LED
    else if (ledNum === 3) {
      if (ledValue === '1') {
        hide_image('blue-off');
        show_image('blue-on');

        // Set the checkbox to match.
        $('#blue-checkbox').prop('checked', true);

        led3 = "ON"
      } else if (ledValue === '0') {
        hide_image('blue-on');
        show_image('blue-off');

        // Set the checkbox to match.
        $('#blue-checkbox').prop('checked', false);

        led3 = "OFF"
      }
    }
  }





  // Let's read the current LED state
  function initial_conditions() {
    var d = $.Deferred();

    setTimeout(function() {

      // Red LED
      $.get('/leds/1',function(data){
        led1 = $.trim(data.split(':')[1]);
      });

      // Green LED
      $.get('/leds/2',function(data){
        led2 = $.trim(data.split(':')[1]);
      });

      // Blue LED
      $.get('/leds/3',function(data){
        led3 = $.trim(data.split(':')[1]);
      });

      // console.log("Got my data now!");
      d.resolve(); // okay, done.
    }, 10); // 10 milliseconds timeout
    return d.done();
  }

  // Let's initialize our LED vars to the current LED state "ON"/"OFF"
  function led_status() {
    var d = $.Deferred();

    setTimeout(function() {
      if (led1 === '0') {
        led1 =  "OFF";
        updateLeds(1,0);
      } else {
        led1 =  "ON";
        updateLeds(1,1);
      }

      if (led2 === '0') {
        led2 =  "OFF";
        updateLeds(2,0);
      } else {
        led2 =  "ON";
        updateLeds(2,1);
      }

      if (led3 === '0') {
        led3 =  "OFF";
        updateLeds(3,0);
      } else {
        led3 =  "ON";
        updateLeds(3,1);
      }

      d.resolve(); // okay got what we need

      console.log("RED:",led1);
      console.log("GRN:",led2);
      console.log("BLU:",led3);
    }, 100); // 100 milliseconds timeout
    return d.promise();
  }


  function firstFunction() {
      var d = $.Deferred();
      // some very time consuming asynchronous code...
      setTimeout(function() {
          console.log('1');
          d.resolve(); // got what we need
      }, 1000); // 1 second timeout

      return d.promise();
  }

  function secondFunction() {
      var d = $.Deferred();
      setTimeout(function() {
                 console.log('2');
                 d.resolve();
      }, 1000); // 1 second timeout
      return d.promise();
  }

  function thirdFunction() {
      var d = $.Deferred();
      setTimeout(function() {
                 console.log('3');
                 d.resolve();
      }, 1000); // 1 second timeout

      return d.promise();
  }

  function fourthFunction() {
      var d = $.Deferred();
      // last function, not executed until the other 3 are done.
      setTimeout(function() {
                 console.log('4');
                 d.resolve();
      }, 1000); // 1 second timeout
      return d.promise();
  }

  // sequence through the synchronous functions using '.then'
  firstFunction().then(thirdFunction).then(secondFunction).then(fourthFunction);

  // make sure to intialize synchronously (10ms back to back -- this is in the setTimeout function)
  initial_conditions().then(led_status);

  // I'm attaching these functions as click actions
  // the same way that we were doing with jquery for the class's code.
  $('#red-checkbox').click(function() {
    if (led1 === "OFF") {led1 = "ON";} else {led1 = "OFF";}
    var params = 'led=1&state='+led1;
    console.log('Led Command with params:' + params);
    $.post('/ledcmd', params, function(data, status){
            console.log("Data: " + data + "\nStatus: " + status);
    });
  });

  $('#green-checkbox').click(function() {
    if (led2 === "OFF") {led2 = "ON";} else {led2 = "OFF";}
    var params = 'led=2&state='+led2;
    console.log('Led Command with params:' + params);
    $.post('/ledcmd', params, function(data, status){
            console.log("Data: " + data + "\nStatus: " + status);
    });
  });

  $('#blue-checkbox').click(function() {
    if (led3 === "OFF") {led3 = "ON";} else {led3 = "OFF";}
    var params = 'led=3&state='+led3;
    console.log('Led Command with params:' + params);
    $.post('/ledcmd', params, function(data, status){
            console.log("Data: " + data + "\nStatus: " + status);
    });
  });

  function hide_image(id_to_hide) {
      var element = document.getElementById(id_to_hide);
      if ( !($(element).hasClass("hide-this"))) {
        $(element).addClass("hide-this");
      }
  }

  function show_image(id_to_show) {
      var element = document.getElementById(id_to_show);
      if ( $(element).hasClass("hide-this")) {
          $(element).removeClass("hide-this");
      }
  }

});
