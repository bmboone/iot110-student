from debouncer import Debouncer
from gpio import PiGpio
    # We just defined this very nice class, PiGpio, in the file
    # (ie the module) gpio (gpio.py).
    # Let's import that class and use it here.

import time # to get millis


# create an instance of the pi gpio driver.
pi_gpio= PiGpio()

# Create an instance of the Debouncer class
debouncer = Debouncer()

print "STARTING DEBOUNCER TEST"

result = debouncer.debounce_using_time_interval(0)
# assert result == 0
print result

result = debouncer.debounce_using_time_interval(1)
# assert result == 0
print result

result = debouncer.debounce_using_time_interval(0)
# assert result == 0
print result

result = debouncer.debounce_using_time_interval(1)
# assert result == 0
print result

result = debouncer.debounce_using_time_interval(1)
# assert result == 0
print result

result = debouncer.debounce_using_time_interval(1)
# assert result == 0
print result

result = debouncer.debounce_using_time_interval(1)
# assert result == 1
print result

result = debouncer.debounce_using_time_interval(1)
# assert result == 1
print result

# Now let's get the switch input from the board
# Let's run this code for about 10 seconds.
print "You have 10 seconds to test the switch by hand..."
start_time = int(round(time.time()))

current_time = int(round(time.time()))
seconds_to_run_test = 10
prev_switch = pi_gpio.read_switch()
prev_result = debouncer.debounce_using_time_interval(prev_switch)

print "Current switch is " + str(prev_switch)

while (current_time - start_time < seconds_to_run_test):
    current_time = int(round(time.time()))
    switch = pi_gpio.read_switch()

    if (switch != prev_switch):
        print "Debouncing!"
        result = debouncer.debounce_using_time_interval(switch)

        if (result != prev_result):
            print "---> Switch changed"
            print "New switch is: " + str(switch)
            prev_result = result
            prev_switch = switch
        else:
            print "---> Waiting until the switch stabilizes"





print "ENDING DEBOUNCER TEST"
