#!/usr/bin/python

import RPi.GPIO as GPIO

PIN_LEDS = [0, 18, 13, 23]
PIN_SW   = 27

class PiGpio(object):
  """RPi Network 'IoT GPIO'"""

  def __init__(self):
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(PIN_LEDS[1], GPIO.OUT)
    GPIO.setup(PIN_LEDS[2], GPIO.OUT)
    GPIO.setup(PIN_LEDS[3], GPIO.OUT)
    GPIO.setup(PIN_SW, GPIO.IN, pull_up_down=GPIO.PUD_UP)

  def get_switch(self):
    return not GPIO.input(PIN_SW)

  def get_led(self, ledID):
    return GPIO.input(PIN_LEDS[ledID])

  def set_led(self, ledID, value):
    GPIO.output(PIN_LEDS[ledID], value)

  def toggle_led(self, ledID):
    self.set_led(ledID, not self.get_led(ledID))
  
