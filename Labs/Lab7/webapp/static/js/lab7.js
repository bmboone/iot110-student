$(document).ready(function() {

    var max_data_saved = 4;
    //Add variable to hold data between receive data events
    var sse_sensor_data = [];

    // the key event receiver function
    iotSource.onmessage = function(e) {
        // parse sse received data
        // console.log(e.data);
        var parsed_json_data = JSON.parse(e.data);

        // For now just log to console
        // console.log(parsed_json_data);

        // Convert string datetime to JS Date object
        parsed_json_data['meas_time'] = new Date(parsed_json_data['meas_time']);
        // console.log(parsed_json_data);

        // Push new data to front of saved data list
        sse_sensor_data.unshift(parsed_json_data);

        // Then remove oldest data up to maximum data saved
        while (sse_sensor_data.length > max_data_saved) { sse_sensor_data.pop(); }

        //updateEnvironmentalTable();
        //updateSwitchIndicator();

        //updateStepperMotor(parsed_json_data);
    }

    // Buttons
    $('#motor_start').click(function() {
        console.log('Start Motor Up!');
        $.get('/motor/1');
    });
    // $('#motor_stop').click() ...

    // $('#motor_zero').click() ...

    // $('#motor_multistep').click() ...


    // Text Fields
    $('#motor_speed').change(function() {
        console.log('Changed motor speed to ' + $('#motor_speed').val());
        $.get('/motor_speed/' + $('#motor_speed').val());
    });
    // $('#motor_position').change() ...

    // $('#motor_steps').change() ...

    // $('#motor_direction').change() ...


    // ============================ STEPPER MOTOR ===============================
    function updateStepperMotor() {
        // ... add code here ...
    }
    // ============================ STEPPER MOTOR ===============================

    // ============================== ENV TABLE =================================

    function updateEnvironmentalTable() {
        // ... add code here ...
    }
    // ============================== / ENV TABLE ===============================

    // Renders the jQuery-ui elements
    $("#tabs").tabs();

    // ===================================================================
    // LED 1 SLIDER
    $("#slider1").slider({
        orientation: "vertical",
        min: 0,
        max: 100,
        value: 50,
        animate: true,
        slide: function(event, ui) {
            var dcValue = ui.value;
            $("#pwm1").val(dcValue);
            console.log("red led duty cycle(%):", ui.value);
            $.post('/set_pwm', { dutyCycle: ui.value });
        }
    });
    $("#pwm1").val($("#slider1").slider("value"));

    // ===================================================================
    // LED 2 SLIDER
    $("#slider2").slider({
        // ... add code here ...
    });
    $("#pwm2").val($("#slider2").slider("value"));
    // ===================================================================

});