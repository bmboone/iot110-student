# Barry Boone
# October 22, 2017
# This is for Lab2
# It's all based on the examples from gitlab.

from gpio import PiGpio # Taking advantage of our nice PiGpio helper class in gpio.py
from flask import *

app = Flask(__name__)
pi_gpio = PiGpio()

# This is the default route and will render the index.html template
# when referenced from my pi, which will be: http://10.0.1.37:5000

@app.route("/")
def index():
    # create an instance of my pi gpio object class.
    pi_gpio = PiGpio()
    
    # The board has a switch and also 3 leds.
    switch_state = pi_gpio.read_switch()
    led1_state = pi_gpio.get_led(1)
    led2_state = pi_gpio.get_led(2)
    led3_state = pi_gpio.get_led(3)
    
    # Render the template and also send it (any number of) local variables.
    return render_template('index.html', switch=switch_state,
                                led1=led1_state,
                                led2=led2_state,
                                led3=led3_state)


# Read the LED status, on or off
# curl http://10.0.1.37:5000/leds/1
# curl http://10.0.1.37:5000/leds/2
# curl http://10.0.1.37:5000/leds/3
# But we can also go to the url: http://10.0.1.37/leds/1 2 or 3

@app.route("/leds/<int:led_state>", methods=['GET'])
def leds(led_state):
  return "LED State:" + str(pi_gpio.get_led(led_state)) + "\n"



# Get whether the switch is on or off
# curl http://10.0.1.37:5000/sw
#
# or just go there in the browser.

@app.route("/sw", methods=['GET'])
def sw():
  return "Switch State:" + str(pi_gpio.read_switch()) + "\n"



# set the LED state by POST method from curl. For example:
# curl --data 'led=1&state=ON' http://10.0.1.37:5000/ledcmd
#
# Or via the browser: http://10.0.1.37:5000/ledcmd?led=1&state=ON

@app.route("/ledcmd", methods=['POST'])
def ledcommand():
    cmd_data = request.data
    print "LED Command:" + cmd_data
    led = int(str(request.form['led']))
    state = str(request.form['state'])
    if(state == 'OFF'):
        pi_gpio.set_led(led,False)
    elif (state == 'ON'):
        pi_gpio.set_led(led,True)
    else:
        return "Argument Error"

    return "Led State Command:" + state + " for LED number:"+ str(led) + "\n"




if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
