#!/usr/bin/env python3
# Barry Boone
# October 13, 2017
# 
# First assignment for IoT 110, which was to get 
# our RPi up and running, get a gitlab account, and
# be able to get a Flask page server going on the RPi.
#
# 1. Flashed an SD card with Raspbian
# 2. Booted up the Pi (set local preferences, turned on SSH, changed
#    the default password, etc.)
# 3. Set up a gitlab account under bmboone, including putting in my public 
#    ssh rsa key. Set up a new (blank) project called iot110-student under
#    my new account.
# 4. Got the original code for the class like this:
#    git clone https://gitlab.com/iot110/iot110-student.git
# 5. Cloned from my new (bmboone) iot110-student epository onto my Mac, set up the 
#    Lab/Lab1 directory structure, and copied in the code
#    retrieved from the iot110 repository for Lab1 into a new Python file.
# 6. Experimented, made changes to the code, set up other pages to serve.
# 7. Tested it on the Mac, then used scp to put it onto the Pi and run and test the Flask 
#    server code (this file) there.
# 8. On the Mac, went to the webpages defined in this server code via the Pi's
#    local IP address and the 5000 port, like this: http://10.0.1.37:5000
# 9. Added the code via git to the local repository and pushed it up to gitlab.

# Decided to experiment more with Flask.
# https://pythonspot.com/en/flask-web-app-with-python/

# Notes:
# - Templates go into the templates subdirectory
# 
# - Be sure to import the various things you need from flask to use it, such as render_template.


from flask import Flask, flash, redirect, render_template, request, session, abort

import socket
from datetime import datetime

# A global variable to test counting how many pages were served
# since the last time this Flask server was restarted
pagecount = 0

## Get my machine hostname
if socket.gethostname().find('.') >= 0:
    hostname=socket.gethostname()
else:
    hostname=socket.gethostbyaddr(socket.gethostname())[0]

app = Flask(__name__)

# A default route -- just returns a hello message.
@app.route("/")
def default():
  global pagecount
  pagecount += 1
  return "Hello IoT World from RPi3: " + hostname
  
# Sending data to a route via putting it into the url,
# and using it in the template. I found I can organize the templates
# into subdirectories which I'm sure will be helpful in keeping the web app
# maintainable.
@app.route("/hello/<string:name>/")
def hello(name):
  test = "TestVar" # Just testing sending all the variables local to this method to the template, and it works.
  return render_template('basic/hello1.html', **locals())
    
# Experiment to return the current time.
@app.route("/time")
def time():
  global pagecount
  pagecount += 1
  return "The current time is: " + datetime.now().strftime('%Y-%m-%d %H:%M:%S')

# Experiment to show how many pages were served since last server restart.
@app.route("/count")
def count():
  global pagecount
  pagecount += 1
  return "Page views since last server restart: " + str(pagecount)        

# Run the website and make sure to make
# by default it externally visible with 0.0.0.0:5000 (default port)
# (My RPi has a local IP address of 10.0.1.37)
if __name__ == "__main__":
    app.run(host='0.0.0.0') 
        # , port=80) # I can assign it to port=80 but only as root, launched via sudo.
        # Ports below 1024 require sudo
        